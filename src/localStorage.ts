import { LoginInfo, Lokasjon } from './interfaces/models';
import { useLocalStorage } from 'usehooks-ts'
import { SetLokasjonId } from './api/api.config';

export function useLogin() {
    const [loginInfo, setLoginInfo] = useLocalStorage<LoginInfo|null>('loginInfo', null);
    const [lokasjon, setLokasjon] = useLocalStorage<Lokasjon|null>("lokasjon", null)

    const login = (loginInfo: LoginInfo) => {
        setLoginInfo(loginInfo)
        setLokasjon(loginInfo.bruker.lokasjon)
        SetLokasjonId(loginInfo.bruker.lokasjon.id);
    };

    const changeLokasjon = (newLokasjon : Lokasjon) =>{
        setLokasjon(newLokasjon)
        SetLokasjonId(newLokasjon.id) //Doesnt seem to be necessary as it is always overrided by isLoggedIn ? 
    }

    const logout = () => {
        setLoginInfo(null);
        setLokasjon(null)
    }

    const isLoggedIn = () => {
        if (!loginInfo || !lokasjon) {
            return false;
        }

        // Request new login one hour before token expires to minimize
        // risk of expiring while filling out form
        const ONE_HOUR = 1000 * 60 * 60;
        if (Date.parse(loginInfo.expires) - ONE_HOUR < Date.now()) {
            logout();
            return false;
        }

        SetLokasjonId(lokasjon.id);

        return true;
    }

    return { login, logout, loginInfo, isLoggedIn, changeLokasjon, lokasjon };
}
