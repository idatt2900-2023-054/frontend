import Layout from './pages/Layout';
import Innleveringer from './pages/Innleveringer';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import NyInnleveringer from './pages/NyInnlevering';
import NyUtlevering from './pages/NyUtlevering';
import Utleveringer from './pages/Utleveringer';
import NotFound from './pages/NotFound';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import Admin from './pages/Admin';
import { useLogin } from './localStorage';

const theme = createTheme({
  palette: {
    primary: {
      main: '#096655'
    },
    background: {
      paper: '#ffffff'
    },
    text: {
      primary: '#096655',
      secondary: '#000000',
    }
  },
});


const App = () => {

  const { loginInfo } = useLogin(); 

  return (
    <ThemeProvider theme={theme}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Layout />}>
              <Route index element={<Innleveringer />} />
              <Route path="ny-innlevering" element={<NyInnleveringer />} />
              <Route path="ny-utlevering" element={<NyUtlevering />} />
              <Route path="innleveringer" element={<Innleveringer />} />
              <Route path="utleveringer" element={<Utleveringer />} />
              {(loginInfo?.bruker.rolle === "Admin" || loginInfo?.bruker.rolle === "SuperAdmin") &&
                <Route path="admin" element={<Admin />} />}
              <Route path="*" element={<NotFound/>} />
            </Route>
          </Routes>
        </BrowserRouter>
    </ThemeProvider>
  )
}

export default App
