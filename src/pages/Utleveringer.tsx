import { Organisasjon, Utlevering, Vektgruppe } from '../interfaces/models';
import { useState } from 'react';
import { deleteUtlevering, getAllUtleveringerOnLokasjon, updateUtlevering } from '../api/utlevering'
import EditableTable from '../components/table/EditableTable';
import Datepicker from '../components/form/DatePicker';
import Select from '../components/form/Select';
import DecimalField from '../components/form/DecimalField';
import IntegerField from '../components/form/IntegerField';
import Checkbox from '../components/form/Checkbox';
import { getOrganisasjoner } from '../api/organisasjon';
import { getVektgrupperOnLokasjon } from '../api/vektgruppe';
import { EditUtleveringForm, EditUtleveringFormSchema } from '../interfaces/forms';
import DynamicTableRow from '../components/table/DynamicTableRow';
import Hover from '../components/Hover';
import { Typography } from '@mui/material';
import AnnouncementIcon from '@mui/icons-material/Announcement';
import TextField from '../components/form/TextField';
import DateRange from '../components/DateRange';

const Utleveringer = () => {
  const [utleveringer, setUtleveringer] = useState<Utlevering[]>([]);
  const [organisasjoner, setOrganisasjoner] = useState<Organisasjon[]>([]);
  const [vektgrupper, setVektgrupper] = useState<Vektgruppe[]>([]);

  const today = new Date();
  const startOfMonth = new Date(Date.UTC(today.getFullYear(), today.getUTCMonth(), 1));
  const [startDate, setStartDate] = useState<Date>(startOfMonth)
  const [endDate, setEndDate] = useState<Date>(today)

  const onInit = async () => {
    await getAllUtleveringerOnLokasjon(startDate, endDate).then(setUtleveringer);
  }

  const onInitEdit = async () => {
    if (organisasjoner.length == 0 || vektgrupper.length == 0) {
      await Promise.all([
        getOrganisasjoner().then(setOrganisasjoner),
        getVektgrupperOnLokasjon().then(setVektgrupper)
      ]);
    }
  }

  const onDelete = async (utlevering: Utlevering) => {
    await deleteUtlevering(utlevering.id);
  }

  const onEdit = async (utlevering: Utlevering, utleveringForm: EditUtleveringForm) => {
    await updateUtlevering(utlevering.id, {
      ...utleveringForm,
      organisasjonId: utleveringForm.organisasjon.id,  
      vektgruppeId: utleveringForm.vektgruppe.id,
    });
  }

  return (
    <div style={{display:"flex", flexDirection:"column", justifyContent:"center", marginBottom:"100px"}}>
      <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between", alignItems: "flex-end", margin: "35px 0" }}>
        <h1 style={{margin: 0}}>Tidligere Utleveringer</h1>
        <DateRange
          startDate={startDate}
          setStartDate={setStartDate}
          endDate={endDate}
          setEndDate={setEndDate}
        />
      </div>
      <EditableTable
        addOnLastRow={false}
        values={utleveringer}
        titles={["Dato", "Organisasjon", "Vektgruppe", "Bruttovekt [kg]", "Kasser Ut", "Kassevekt [kg]", "Nettovekt [kg]", "Kasser Inn", "Primær", "Notat"]}
        formSchema={EditUtleveringFormSchema}
        deleteConfirmationPrompt="Er du sikker på at du vil slette utleveringen?"
        dependencies={[startDate, endDate]}
        onInit={onInit}
        onInitEdit={onInitEdit}
        onDelete={onDelete}
        onEdit={onEdit}
        NormalRow={props => 
          <DynamicTableRow
            id={props.value.id.toString()}
            keys={props.keys}
            values={[
              props.value.dato,
              props.value.organisasjon.navn,
              props.value.vektgruppe.navn,
              props.value.bruttovekt.toFixed(2),
              props.value.kasserUt,
              props.value.kassevekt.toFixed(2),
              props.value.nettovekt.toFixed(2),
              props.value.kasserInn,
              props.value.primaer,

              props.value.notat ?
              <Hover
                VisibleElement={() => 
                  <AnnouncementIcon/>
                }
                HiddenElement={() =>
                  <Typography color="black" variant="caption" display="block" sx={{width: "300px", padding:"10px"}}>{props.value.notat}</Typography>
                }
              /> : "",

              ...props.buttons
            ]}
          />
        }
        EditableRow={props =>
          <DynamicTableRow
            id="EditableUtlevering"
            keys={props.keys}
            values={[
              <Datepicker id="dato" control={props.control} defaultValue={props.value?.dato} />,
              <Select
                id="organisasjon"
                control={props.control}
                options={organisasjoner}
                getOptionLabel={organisasjon => organisasjon.navn}
                defaultValue={props.value?.organisasjon}
              />,
              <Select
                id="vektgruppe"
                control={props.control}
                options={vektgrupper}
                getOptionLabel={vektgruppe => vektgruppe.navn}
                defaultValue={props.value?.vektgruppe}
              />,
              <DecimalField
                id="bruttovekt"
                control={props.control}
                defaultValue={props.value?.bruttovekt}
              />,
              <IntegerField
                id="kasserInn"
                control={props.control}
                defaultValue={props.value?.kasserInn}
              />,
              <DecimalField
                id="kassevekt"
                control={props.control}
                defaultValue={props.value?.kassevekt}
              />,
              <p></p>,
              <IntegerField
                id="kasserUt"
                control={props.control}
                defaultValue={props.value?.kasserUt}
              />,
              <Checkbox
                id="primaer"
                control={props.control}
                defaultValue={props.value?.primaer}
              />,
              <TextField
                id="notat"
                control={props.control}
                defaultValue={props.value?.notat}
                multiline={true}
                sx={{width: "300px"}}
              />,
              ...props.buttons
            ]}
          />
        }
      />
    </div>
  )
}
  
export default Utleveringer;