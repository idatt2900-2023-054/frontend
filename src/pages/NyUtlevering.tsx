import { Button, Box } from "@mui/material";
import Divider from "@mui/material/Divider";
import { useEffect, useRef, useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { addUtleveringer } from "../api/utlevering";
import { getOrganisasjoner } from "../api/organisasjon";
import { getVektgrupperOnLokasjon } from "../api/vektgruppe";
import Datepicker from "../components/form/DatePicker";
import OrganisasjonTable from "../components/nyUtlevering/OrganisasjonTable";
import { Organisasjon, Vektgruppe } from "../interfaces/models";
import { RegisterUtleveringRequest } from "../interfaces/requests";
import { UtleveringForm, UtleveringFormSchema } from "../interfaces/forms";
import { useNavigate } from "react-router-dom";
import ConfirmationDialog from "../components/form/ConfirmationDialog";
import UseLoading from "../components/Loading";
import { yupResolver } from "@hookform/resolvers/yup";

const NyUtlevering = () => {
  const [organisasjoner, setOrganisasjoner] = useState<Organisasjon[]>([]);
  const [vektgrupper, setVektgrupper] = useState<Vektgruppe[]>([]);
  const [numOrganisasjoner, setNumOrganisasjoner] = useState<number>(1);
  const [removeOrganisasjonIndex, setRemoveOrganisasjonIndex] = useState<number>(0);
  let { current: expandedArray } = useRef<boolean[]>([true]);

  const methods = useForm<UtleveringForm>({resolver: yupResolver(UtleveringFormSchema)});

  const onExpandedChange = (index: number, expanded: boolean) => {
    expandedArray[index] = expanded;
  }

  const addOrganisasjon = (index: number) => {
    setNumOrganisasjoner(index + 1)
    expandedArray.push(true)
  }

  const navigate = useNavigate();

  const { Loading, DoLoadingOperation } = UseLoading(true);

  useEffect(() => {
    DoLoadingOperation(async () => {
      await Promise.all([
        getOrganisasjoner().then(setOrganisasjoner),
        getVektgrupperOnLokasjon().then(setVektgrupper)
      ])
    });
  }, []);

    //Get values from the form. When used during handleRemove, pop organisasjoner through slicing.
  const cleanupOrganisajoner = (newLength: number) => {
    const organisasjonForms = methods.getValues(`organisasjoner`)
    if (organisasjonForms?.length > newLength) {
      const sliced = organisasjonForms.slice(0, newLength);  
      methods.setValue(`organisasjoner`, sliced);
    }
  }


  // Delete an organisasjon by taking all values to the right of the deleted organisasjon, and shifting them one index to the left.
  // Leaves a duplicate at the end of the organisasjoner state array, which is later popped in cleanupOrganisasjoner.
  // Resets removeOrganisasjonIndex and updates expandedArray and numOrganisasjoner accordingly.
  const handleRemove = () => {
    for (let i = removeOrganisasjonIndex!; i < numOrganisasjoner; i++) {
      const values = methods.getValues(`organisasjoner.${i + 1}`);
      methods.unregister(`organisasjoner.${i}`);
      methods.setValue(`organisasjoner.${i}`, values);
    }
    expandedArray.splice(removeOrganisasjonIndex, 1);
    setRemoveOrganisasjonIndex(0);
    const newLength = numOrganisasjoner - 1;
    setNumOrganisasjoner(newLength);
    cleanupOrganisajoner(newLength);
  };

  //Translate form state to request.
  const toRequests = (utleveringForm: UtleveringForm): RegisterUtleveringRequest[] => {
    const requests: RegisterUtleveringRequest[] = [];
    utleveringForm.organisasjoner.forEach((organisasjonForm) => {
      let kasserInnRegistered = false;
      if (organisasjonForm?.vektgrupper) {
        organisasjonForm.vektgrupper.forEach((vektgruppeForm) => {
          if (vektgruppeForm?.vektgruppe) {
            requests.push({
              dato: utleveringForm.dato,

              organisasjonId: organisasjonForm.organisasjon.id,
              primaer: organisasjonForm.primaer,
              kasserInn: !kasserInnRegistered ? organisasjonForm.kasserInn : 0,

              vektgruppeId: vektgruppeForm.vektgruppe.id,
              //Sum bruttovekter and kasserInn
              bruttovekt: vektgruppeForm.bruttovekter.reduce((a, b) => (b ? a + b : a), 0),
              kasserUt: vektgruppeForm.kasserUt.reduce((a, b) => (b ? a + b : a), 0),
              kassevekt: vektgruppeForm.kassevekt,
              notat: vektgruppeForm. notat
            });

            kasserInnRegistered = true;
          }
        });
      }
    });
    return requests;
  };

  const onSubmit = methods.handleSubmit((values) => {
    DoLoadingOperation(async () => {
      const requests = toRequests(values);
      await addUtleveringer(requests).then(() => navigate("/utleveringer"))
    });
  }, console.error);

  //Prevent Enter from submitting the form
  const handleKeyDown = (e: React.KeyboardEvent<HTMLFormElement> & {target: {nodeName?: string}}) => {
    if(e.key == "Enter" && e.target?.nodeName !== 'BUTTON'){
      e.preventDefault();
    }
  }

  return (
    <>
      <FormProvider {...methods}>
        <form onSubmit={onSubmit} onKeyDown={handleKeyDown}>
          <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between", alignItems: "flex-end", margin: "35px 0" }}>
            <h1 style={{ margin: "0" }}>Ny Utlevering</h1>
            <Datepicker id="dato" control={methods.control} defaultValue={new Date()} />
          </div>

          <Loading><>
            {Array.from({ length: numOrganisasjoner }).map((x, i) => (
              <>
                <OrganisasjonTable 
                key={i} 
                organisasjonIndex={i} 
                numOrganisasjoner={numOrganisasjoner} 
                setRemoveOrganisasjonIndex={setRemoveOrganisasjonIndex} 
                organisasjoner={organisasjoner} 
                vektgrupper={vektgrupper}
                expanded={expandedArray[i]}
                onExpandedChange={onExpandedChange} />
                {i + 1 != numOrganisasjoner && <Divider sx={{ marginTop: "25px", marginBottom: "25px", borderWidth: "2px", borderRadius: "2px", backgroundColor: "text.primary", width: "calc(100% + 50px)", position: "relative", left: "-25px" }}></Divider>}
              </>
            ))}

            <Box sx={{ marginTop: "50px", marginBottom: "50px", display: "flex", flexDirection: "row", justifyContent: "space-between" }}>
              <Button onClick={() => addOrganisasjon(numOrganisasjoner)} variant="outlined">
                Legg til organisasjon
              </Button>
              <Button type="submit" variant="outlined">
                Registrer Utlevering{numOrganisasjoner > 1 && "er"}
              </Button>
            </Box>
          </></Loading>
        </form>
      </FormProvider>

      <ConfirmationDialog
        open={removeOrganisasjonIndex != 0}
        prompt="Er du sikker på at du vil fjerne organisasjonen?"
        onCancel={() => setRemoveOrganisasjonIndex(0)}
        onConfirm={() => handleRemove()}
      />
    </>
  );
};

export default NyUtlevering;
