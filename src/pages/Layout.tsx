import { Outlet } from 'react-router-dom';
import Login from './Login';
import Menu from '../components/Menu';
import Box from '@mui/material/Box';
import { useLogin } from '../localStorage';
import { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import ErrorStack from '../components/ErrorStack';
import ErrorContextProvider from '../ErrorContext';

function Layout() {
  const [loggedIn, setLoggedIn] = useState(false);
  const { isLoggedIn, loginInfo } = useLogin();
  const location = useLocation();

  useEffect(() => {
    setLoggedIn(isLoggedIn())
  }, [loginInfo, location])

  if (loggedIn)
  {
    return (
      <ErrorContextProvider>
        <Menu/>
        <ErrorStack/>
        <div style={{
          width: "calc(100% - 150px)",
          minHeight: "100%",
          marginLeft: "150px"
        }}>
          <Box sx={{padding: "0 50px", color:"text.primary", maxWidth: "2000px", margin: "auto"}}>
            <Outlet/>
          </Box>
        </div>
      </ErrorContextProvider>
    )
  }
  else
  {
    return (
      <Login/>
    )
  }
}
  
export default Layout