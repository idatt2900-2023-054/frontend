import { Button, Box } from "@mui/material";
import Divider from "@mui/material/Divider";
import { useEffect, useRef, useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { addInnleveringer } from "../api/innlevering";
import { getAktiveLeverandorer } from "../api/leverandor";
import { getVaregrupperOnLokasjon } from "../api/varegruppe";
import Datepicker from "../components/form/DatePicker";
import LeverandorTable from "../components/nyInnlevering/LeverandorTable";
import { Leverandor, Varegruppe } from "../interfaces/models";
import { RegisterInnleveringRequest } from "../interfaces/requests";
import { InnleveringForm, InnleveringFormSchema } from "../interfaces/forms";
import { useNavigate } from "react-router-dom";
import ConfirmationDialog from "../components/form/ConfirmationDialog";
import UseLoading from "../components/Loading";
import { yupResolver } from '@hookform/resolvers/yup';

const NyInnlevering = () => {
  const [leverandorer, setLeverandorer] = useState<Leverandor[]>([]);
  const [varegrupper, setVaregrupper] = useState<Varegruppe[]>([]);
  const [numLeverandorer, setNumLeverandorer] = useState<number>(1);
  const [removeLeverandorIndex, setRemoveLeverandorIndex] = useState<number>(0);
  let { current: expandedArray } = useRef<boolean[]>([true]);

  const methods = useForm<InnleveringForm>({mode: "onTouched", resolver: yupResolver(InnleveringFormSchema)});

  const onExpandedChange = (index: number, expanded: boolean) => {
    expandedArray[index] = expanded;
  }

  const addLeverandor = (index: number) => {
    setNumLeverandorer(index + 1)
    expandedArray.push(true)
  }

  const navigate = useNavigate();

  const { Loading, DoLoadingOperation } = UseLoading(true);

  useEffect(() => {
    DoLoadingOperation(async () => {
      await Promise.all([
        getAktiveLeverandorer().then(setLeverandorer),
        getVaregrupperOnLokasjon().then(setVaregrupper)
      ])
    });
  }, []);


  //Get values from the form. When used during handleRemove, pop leverandorer through slicing.
  const cleanupLeverandorer = (newLength: number) => {
    const leverandorForms = methods.getValues(`leverandorer`)
    if (leverandorForms?.length > newLength) {
      const sliced = leverandorForms.slice(0, newLength);  
      methods.setValue(`leverandorer`, sliced);
    }
  }

  // Delete a leverandor by taking all values to the right of the deleted leverandor, and shifting them one index to the left.
  // Leaves a duplicate at the end of the leverandorer state array, which is later popped in cleanupLeverandorer.
  // Resets removeLeverandorIndex and updates expandedArray and numLeverandorer accordingly.
  const handleRemove = () => {
    for (let i = removeLeverandorIndex!; i < numLeverandorer; i++) {
      const values = methods.getValues(`leverandorer.${i + 1}`);
      methods.unregister(`leverandorer.${i}`);
      methods.setValue(`leverandorer.${i}`, values);
    }
    expandedArray.splice(removeLeverandorIndex, 1);
    setRemoveLeverandorIndex(0);
    const newLength = numLeverandorer - 1;
    setNumLeverandorer(newLength);
    cleanupLeverandorer(newLength);
  };

  //Translate form state to request.
  const toRequests = (innleveringForm: InnleveringForm): RegisterInnleveringRequest[] => {
    const requests: RegisterInnleveringRequest[] = [];
    innleveringForm.leverandorer.forEach((leverandorForm) => {
      if (leverandorForm?.varegrupper) {
        leverandorForm.varegrupper.forEach((varegruppeForm) => {
          if (varegruppeForm?.varegruppe) {
            requests.push({
              dato: innleveringForm.dato,
              
              leverandorId: leverandorForm.leverandor.id,
              renDonasjon: leverandorForm.renDonasjon,
              primaer: leverandorForm.primaer,

              varegruppeId: varegruppeForm.varegruppe.id,
              //Sum bruttovekter and kasserInn
              bruttovekt: varegruppeForm.bruttovekter.reduce((a, b) => (b ? a + b : a), 0),
              kasserInn: varegruppeForm.kasserInn.reduce((a, b) => (b ? a + b : a), 0),
              kassevekt: varegruppeForm.kassevekt,
              notat: varegruppeForm.notat
            });            
          }
        });
      }
    });
    return requests;
  };

  const onSubmit = methods.handleSubmit(values => {
    DoLoadingOperation(async () => {
      const requests = toRequests(values);
      await addInnleveringer(requests).then(() => navigate("/innleveringer"))
    });
  }, console.error);
  
  //Prevent Enter from submitting the form
  const handleKeyDown = (e: React.KeyboardEvent<HTMLFormElement> & {target: {nodeName?: string}}) => {
    if(e.key == "Enter" && e.target?.nodeName !== 'BUTTON'){
      e.preventDefault();
    }
  }

  return (
    <>
      <FormProvider {...methods}>
        <form onSubmit={onSubmit} onKeyDown={handleKeyDown}>
          <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between", alignItems: "flex-end", margin: "35px 0" }}>
            <h1 style={{ margin: "0" }}>Ny Innlevering</h1>
            <Datepicker id="dato" control={methods.control} defaultValue={new Date()} />
          </div>
          <Loading><>
            {Array.from({ length: numLeverandorer }).map((x, i) => (
              <>
                <LeverandorTable
                  key={i}
                  leverandorIndex={i}
                  numLeverandorer={numLeverandorer}
                  setRemoveLeverandorIndex={setRemoveLeverandorIndex}
                  leverandorer={leverandorer}
                  varegrupper={varegrupper}
                  expanded={expandedArray[i]}
                  onExpandedChange={onExpandedChange} />
                  {i + 1 != numLeverandorer && <Divider sx={{ marginTop: "25px", marginBottom: "25px", borderWidth: "2px", borderRadius: "2px", backgroundColor: "text.primary", width: "calc(100% + 50px)", position: "relative", left: "-25px" }}></Divider>}
              </>
            ))}

            <Box sx={{ marginTop: "50px", marginBottom: "50px", display: "flex", flexDirection: "row", justifyContent: "space-between" }}>
              <Button onClick={() => addLeverandor(numLeverandorer)} variant="outlined" >
                Legg til leverandør
              </Button>
              <Button type="submit" variant="outlined">
                Registrer Innlevering{numLeverandorer > 1 && "er"}
              </Button>
            </Box>
          </></Loading>
        </form>
      </FormProvider>
      <ConfirmationDialog
        open={removeLeverandorIndex != 0}
        prompt="Er du sikker på at du vil fjerne leverandøren?"
        onCancel={() => setRemoveLeverandorIndex(0)}
        onConfirm={() => handleRemove()}
      />
    </>
  );
};

export default NyInnlevering;
