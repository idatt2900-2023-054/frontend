import Vektgrupper from '../components/admin/Vektgrupper';
import Brukere from '../components/admin/Brukere';
import VaregrupperNasjonal from '../components/admin/VaregrupperNasjonal';
import VaregrupperLokal from '../components/admin/VaregruppeLokal';
import { getAllLokasjoner } from '../api/lokasjon';
import { useState, useEffect } from 'react';
import { Lokasjon } from '../interfaces/models';
import Lokasjoner from '../components/admin/Lokasjoner';
import { useLogin } from '../localStorage';
import { TextField, Autocomplete } from '@mui/material';
import Selskaper from '../components/admin/Selskaper';
import Leverandorer from '../components/admin/Leverandorer';

const Admin = () => {
  const [varegrupperChanged, setVaregrupperChanged] = useState<boolean>(false);
  const [selskaperChanged, setSelskaperChanged] = useState<boolean>(false);
  const [lokasjoner, setLokasjoner] = useState<Lokasjon[]>([]);
  const { loginInfo, changeLokasjon, lokasjon } = useLogin(); 

  return (
    <div style={{display:"flex", flexDirection:"column", textAlign: "left"}}>
      <div style={{display:"flex", flexDirection:"row", textAlign: "left"}}>
        <h1>Admin</h1>
        {/* Lokasjon selector for SuperAdmin */}
        { loginInfo?.bruker.rolle === "SuperAdmin" &&
        <Autocomplete
            disablePortal
            disableClearable
            id="adminLokasjon"
            fullWidth
            defaultValue={lokasjon ? lokasjon : undefined}
            autoHighlight
            options={lokasjoner}
            getOptionLabel={value => value.navn}
            onChange={(event, selectedValue) => {changeLokasjon(selectedValue)}}
            size="small"
            sx={{alignSelf:"center", width: "20%", color: "black", paddingLeft: "25px"}}
            renderInput={(params) =>
              <TextField
                {...params}
                sx={{input: {color: "black"}, border: "1px solid black", borderRadius: "5px"}}
              />}
          />
            }
      </div>
      <div style={{display:"flex", flexDirection:"column", gap:"25px", justifyContent:"center", marginBottom:"100px"}}>

        {(loginInfo?.bruker.rolle === "Admin" || loginInfo?.bruker.rolle === "SuperAdmin") &&
        <>
          <VaregrupperLokal lokasjon={lokasjon} varegrupperChanged={varegrupperChanged} setVaregrupperChanged={setVaregrupperChanged}/>
          <Vektgrupper lokasjon={lokasjon}/>
          <Leverandorer setSelskaperChanged={setSelskaperChanged} selskaperChanged={selskaperChanged}/>
        </>
        }
        { loginInfo?.bruker.rolle === "SuperAdmin" &&
        <>
          <h1 style={{margin: "10px"}}>Nasjonal Admin</h1>
          <Selskaper setSelskaperChanged={setSelskaperChanged}/>
          <VaregrupperNasjonal setVaregrupperChanged={setVaregrupperChanged}/>
          <Lokasjoner lokasjoner={lokasjoner} setLokasjoner={setLokasjoner}/>
          <Brukere lokasjoner={lokasjoner} />
        </>
        }
      </div>
    </div>
  )
}

export default Admin;