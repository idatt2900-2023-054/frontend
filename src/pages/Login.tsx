import { GoogleLogin, CredentialResponse } from '@react-oauth/google';
import { Login as apiLogin } from '../api/login';
import { Box } from '@mui/material';
import { useLogin } from '../localStorage';
import UseLoading from '../components/Loading';

const Login = () => {
  const { login } = useLogin();
  const { Loading, DoLoadingOperation } = UseLoading(false);
  
  const responseMessage = async (credentialResponse: CredentialResponse) => {
    DoLoadingOperation(async () =>{
      const loginResponse = await apiLogin(credentialResponse.credential!);
      login(loginResponse);
    })
  };
  
  const errorMessage = () => {
    console.error();
  };

  return (
    <Box sx={{color:"text.primary", display: "flex", flexDirection:"column", justifyContent:"center", alignItems:"center"}}>
      <h1>Matsentralen Registrering</h1>
      <Loading>
        <GoogleLogin
          onSuccess={responseMessage}
          onError={errorMessage}
          ux_mode="popup"
        />
      </Loading>
    </Box>
  )
}

export default Login;