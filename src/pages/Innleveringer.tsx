import { Innlevering, Leverandor, Varegruppe } from '../interfaces/models';
import { deleteInnlevering, getAllInnleveringerOnLokasjon, updateInnlevering } from '../api/innlevering';
import { getAktiveLeverandorer } from '../api/leverandor';
import { getVaregrupperOnLokasjon } from '../api/varegruppe';
import EditableTable from '../components/table/EditableTable';
import Datepicker from '../components/form/DatePicker';
import Select from '../components/form/Select';
import DecimalField from '../components/form/DecimalField';
import IntegerField from '../components/form/IntegerField';
import Checkbox from '../components/form/Checkbox';
import { useState } from 'react';
import { EditInnleveringForm, EditInnleveringFormSchema } from '../interfaces/forms';
import DynamicTableRow from '../components/table/DynamicTableRow';
import TextField from '../components/form/TextField';
import { Typography } from '@mui/material';
import Hover from '../components/Hover';
import AnnouncementIcon from '@mui/icons-material/Announcement';
import DateRange from '../components/DateRange';

const Innleveringer = () => {
  const [innleveringer, setInnleveringer] = useState<Innlevering[]>([]);
  const [leverandorer, setLeverandorer] = useState<Leverandor[]>([]);
  const [varegrupper, setVaregrupper] = useState<Varegruppe[]>([]);
  const today = new Date();
  const startOfMonth = new Date(Date.UTC(today.getFullYear(), today.getUTCMonth(), 1));
  const [startDate, setStartDate] = useState<Date>(startOfMonth)
  const [endDate, setEndDate] = useState<Date>(today)

  const onInit = async () => {
    await getAllInnleveringerOnLokasjon(startDate, endDate).then(setInnleveringer);
  };

  const onInitEdit = async () => {
    if (leverandorer.length == 0 || varegrupper.length == 0) {
      await Promise.all([
        getAktiveLeverandorer().then(setLeverandorer),
        getVaregrupperOnLokasjon().then(setVaregrupper)
      ]);
    }
  }

  const onDelete = async (innlevering: Innlevering) => {
    await deleteInnlevering(innlevering.id);
  }

  const onEdit = async (innlevering: Innlevering, innleveringForm: EditInnleveringForm) => {
    await updateInnlevering(innlevering.id, {
      ...innleveringForm,
      leverandorId: innleveringForm.leverandor.id,  
      varegruppeId: innleveringForm.varegruppe.id,
    });
  }

  return (
    <div style={{display:"flex", flexDirection:"column", justifyContent:"center", marginBottom:"100px"}}>
      <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between", alignItems: "flex-end", margin: "35px 0" }}>
        <h1 style={{margin: 0}}>Tidligere Innleveringer</h1>
        <DateRange
          startDate={startDate}
          setStartDate={setStartDate}
          endDate={endDate}
          setEndDate={setEndDate}
        />
      </div>

      <EditableTable
        addOnLastRow={false}
        values={innleveringer}
        titles={["Dato", "Leverandør", "Varegruppe", "Bruttovekt [kg]", "Kasser Inn", "Kassevekt [kg]", "Nettovekt [kg]", "Ren Donasjon", "Primær", "Notat"]}
        formSchema={EditInnleveringFormSchema}        
        deleteConfirmationPrompt="Er du sikker på at du vil slette innleveringen?"
        dependencies={[startDate, endDate]}
        onInit={onInit}
        onInitEdit={onInitEdit}
        onDelete={onDelete}
        onEdit={onEdit}
        NormalRow={props =>
          <DynamicTableRow
            id={props.value.id.toString()}
            keys={props.keys}
            values={[
              props.value.dato,
              props.value.leverandor.navn,
              props.value.varegruppe.navn,
              props.value.bruttovekt.toFixed(2),
              props.value.kasserInn,
              props.value.kassevekt.toFixed(2),
              props.value.nettovekt.toFixed(2),
              props.value.renDonasjon,
              props.value.primaer,

              props.value.notat ?
              <Hover
                VisibleElement={() => 
                  <AnnouncementIcon/>
                }
                HiddenElement={() =>
                  <Typography color="black" variant="caption" display="block" sx={{width: "300px", padding:"10px"}}>{props.value.notat}</Typography>
                }
              /> : "",

              ...props.buttons
            ]}
          />
        }
        EditableRow={props =>
          <DynamicTableRow
            id={"EditableInnlevering"}
            keys={props.keys}
            values={[
              <Datepicker id="dato" control={props.control} defaultValue={props.value?.dato} />,
              <Select
                id="leverandor"
                control={props.control}
                options={leverandorer}
                getOptionLabel={leverandor => leverandor.navn}
                defaultValue={props.value?.leverandor}
              />,
              <Select
                id="varegruppe"
                control={props.control}
                options={varegrupper}
                getOptionLabel={varegruppe => varegruppe.navn}
                defaultValue={props.value?.varegruppe}
              />,
              <DecimalField
                id="bruttovekt"
                control={props.control}
                defaultValue={props.value?.bruttovekt}
              />,
              <IntegerField
                id="kasserInn"
                control={props.control}
                defaultValue={props.value?.kasserInn}
              />,
              <DecimalField
                id="kassevekt"
                control={props.control}
                defaultValue={props.value?.kassevekt}
              />,
              <p></p>,
              <Checkbox
                id="renDonasjon"
                control={props.control}
                defaultValue={props.value?.renDonasjon}
              />,
              <Checkbox
                id="primaer"
                control={props.control}
                defaultValue={props.value?.primaer}
              />,
              <TextField
                id="notat"
                control={props.control}
                defaultValue={props.value?.notat}
                multiline={true}
                sx={{width: "300px"}}
              />,
              ...props.buttons
            ]}
          />
        }
      />
    </div>
  )
}
export default Innleveringer;