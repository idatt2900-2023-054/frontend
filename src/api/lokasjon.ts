import axios from './api.config';
import { RegisterLokasjonRequest } from '../interfaces/requests';
import { Lokasjon } from '../interfaces/models';

export const getAllLokasjoner = async (): Promise<Lokasjon[]> => {
  const response = await axios.get(
    `lokasjoner`
  );
  
  return response.data;
};

export const addLokasjon = async (request: RegisterLokasjonRequest): Promise<Lokasjon> => {
  const response = await axios.post(
    `lokasjon`,
    request
  );
  return response.data;
};

export const deleteLokasjon = async (lokasjonId: number): Promise<void> => {
  const response = await axios.delete(
    `lokasjon/${lokasjonId}`,
  );
  return response.data;
};

export const updateLokasjon = async (lokasjonId: number, request: RegisterLokasjonRequest): Promise<Lokasjon> => {
  const response = await axios.put(
    `lokasjon/${lokasjonId}`,
    request
  );
  return response.data;
};