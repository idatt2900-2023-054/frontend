import axios from './api.config';
import { Vektgruppe } from '../interfaces/models';
import { RegisterVektgruppeRequest } from '../interfaces/requests';

export const getVektgrupperOnLokasjon = async (): Promise<Vektgruppe[]> => {
  const response = await axios.get(
    `lokasjon/{lokasjonId}/vektgrupper`
  );
  return response.data;
};

export const addVektgruppe = async (vektgruppe: RegisterVektgruppeRequest): Promise<Vektgruppe> => {
  const response = await axios.post(
    `lokasjon/{lokasjonId}/vektgruppe`,
    vektgruppe
  );
  return response.data;
};

export const updateVektgruppe = async (vektgruppeId: number, vektgruppe: RegisterVektgruppeRequest): Promise<Vektgruppe> => {
  const response = await axios.put(
    `lokasjon/{lokasjonId}/vektgruppe/${vektgruppeId}`,
    vektgruppe
  );
  return response.data;
};

export const deleteVektgruppe = async (vektgruppeId: number): Promise<void> => {
  await axios.delete(
    `lokasjon/{lokasjonId}/vektgruppe/${vektgruppeId}`
  );
};