import axios from './api.config';
import { Leverandor } from '../interfaces/models';
import { RegisterLeverandorRequest } from '../interfaces/requests';

export const getLeverandorer = async (): Promise<Leverandor[]> => {
  const response = await axios.get(
    `leverandorer`
    );
  return response.data;
};

export const getAktiveLeverandorer = async (): Promise<Leverandor[]> => {
  const response = await axios.get(
    `leverandorer/aktive`
    );
  return response.data;
};

export const addLeverandor = async (request: RegisterLeverandorRequest): Promise<Leverandor> => {
  const response = await axios.post(
    `leverandorer`,
    request
  );
  return response.data;
};

export const deleteLeverandor = async (leverandorId: number): Promise<void> => {
  const response = await axios.delete(
    `leverandorer/${leverandorId}`,
  );
  return response.data;
};

export const updateLeverandor = async (leverandorId: number, request: RegisterLeverandorRequest): Promise<Leverandor> => {
  const response = await axios.put(
    `leverandorer/${leverandorId}`,
    request
  );
  return response.data;
};