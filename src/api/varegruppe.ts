import axios from './api.config';
import { Varegruppe, VaregruppeLokasjonRecursive, VaregruppeRecursive } from '../interfaces/models';
import { RegisterVaregruppeRequest } from '../interfaces/requests';

export const getVaregrupperOnLokasjon = async (): Promise<Varegruppe[]> => {
  const response = await axios.get(
    `lokasjon/{lokasjonId}/varegrupper`
    );
  return response.data;
};

export const getAllVaregrupper = async (): Promise<Varegruppe[]> => {
  const response = await axios.get(
    `varegrupper`
  );
  return response.data;
};

export const addVaregruppe = async (varegruppe: RegisterVaregruppeRequest): Promise<Varegruppe> => {
  const response = await axios.post(
    `varegruppe`,
    varegruppe
  );
  return response.data;
};


export const updateVaregruppe= async (varegruppeId: number, varegruppe: RegisterVaregruppeRequest): Promise<Varegruppe> => {
  const response = await axios.put(
    `varegruppe/${varegruppeId}`,
    varegruppe
  );
  return response.data;
};

export const getHovedgrupper = async (): Promise<VaregruppeRecursive[]> => {
  const response = await axios.get(
    `hovedgrupper`
  );
  return response.data;
};

export const getHovedgrupperLokasjon = async (): Promise<VaregruppeLokasjonRecursive[]> => {
  const response = await axios.get(
    `lokasjon/{lokasjonId}/hovedgrupper`
  );
  return response.data;
};


export const deleteVaregruppe = async (varegruppeId: number): Promise<void> => {
  await axios.delete(
    `varegruppe/${varegruppeId}`
  );
};

export const addVaregruppeLokasjon = async (varegruppeId: number): Promise<void> => {
  await axios.post(
    `lokasjon/{lokasjonId}/varegruppe/${varegruppeId}`
  );
};

export const deleteVaregruppeLokasjon = async (varegruppeId: number): Promise<void> => {
  await axios.delete(
    `lokasjon/{lokasjonId}/varegruppe/${varegruppeId}`
  );
};