import axios from './api.config';
import { LoginInfo } from '../interfaces/models';

export const Login = async (credential: string): Promise<LoginInfo> => {
  const response = await axios.post(
    `login`,
    {
      credential: credential
    }
  );

  return response.data;
};

export const Logout = async () => {
  await axios.post(
    `logout`
  );
};