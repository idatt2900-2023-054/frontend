import axios, { toDateOnlyString } from './api.config';
import { Innlevering, Utlevering } from '../interfaces/models';
import { RegisterUtleveringRequest } from '../interfaces/requests';

export const getAllUtleveringerOnLokasjon = async (startDate: Date, endDate: Date): Promise<Utlevering[]> => {
  const response = await axios.get(
    `lokasjon/{lokasjonId}/utleveringer?startDate=${toDateOnlyString(startDate)}&endDate=${toDateOnlyString(endDate)}`
  );

  return response.data;
};

export const addUtleveringer = async (utleveringer: RegisterUtleveringRequest[]): Promise<Utlevering> => {
  const response = await axios.post(
    `lokasjon/{lokasjonId}/utlevering`,
    utleveringer
  );

  return response.data;
};

export const updateUtlevering = async (utleveringId: number, utlevering: RegisterUtleveringRequest): Promise<Innlevering> => {
  const response = await axios.put(
    `lokasjon/{lokasjonId}/utlevering/${utleveringId}`,
    utlevering
  );

  return response.data;
};

export const deleteUtlevering = async (utleveringId: number): Promise<void> => {
  await axios.delete(
    `lokasjon/{lokasjonId}/utlevering/${utleveringId}`
  );
};
