import axios from './api.config';
import { Selskap } from '../interfaces/models';
import { RegisterSelskapRequest } from '../interfaces/requests';

export const getSelskaper = async (): Promise<Selskap[]> => {
  const response = await axios.get(
    `selskap`
    );
  return response.data;
};

export const addSelskap = async (request: RegisterSelskapRequest): Promise<Selskap> => {
    const response = await axios.post(
      `selskap`,
      request
    );
    return response.data;
  };
  
  export const deleteSelskap = async (selskapId: number): Promise<void> => {
    const response = await axios.delete(
      `selskap/${selskapId}`,
    );
    return response.data;
  };
  
  export const updateSelskap = async (selskapId: number, request: RegisterSelskapRequest): Promise<Selskap> => {
    const response = await axios.put(
      `selskap/${selskapId}`,
      request
    );
    return response.data;
  };