import axios, { toDateOnlyString } from './api.config';
import { Innlevering } from '../interfaces/models';
import { RegisterInnleveringRequest } from '../interfaces/requests';

export const getAllInnleveringerOnLokasjon = async (startDate: Date, endDate: Date): Promise<Innlevering[]> => {
  const response = await axios.get(
    `lokasjon/{lokasjonId}/innleveringer?startDate=${toDateOnlyString(startDate)}&endDate=${toDateOnlyString(endDate)}`
  );

  return response.data;
};

export const addInnleveringer = async (innleveringer: RegisterInnleveringRequest[]): Promise<Innlevering[]> => {
  const response = await axios.post(
    `lokasjon/{lokasjonId}/innlevering`,
    innleveringer
  );

  return response.data;
};

export const updateInnlevering = async (innleveringId: number, innlevering: RegisterInnleveringRequest): Promise<Innlevering> => {
  const response = await axios.put(
    `lokasjon/{lokasjonId}/innlevering/${innleveringId}`,
    innlevering
  );

  return response.data;
};

export const deleteInnlevering = async (innleveringId: number): Promise<void> => {
  await axios.delete(
    `lokasjon/{lokasjonId}/innlevering/${innleveringId}`
  );
};