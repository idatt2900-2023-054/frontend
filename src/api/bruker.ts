import axios from './api.config';
import { Bruker } from '../interfaces/models';
import { RegisterBrukerRequest } from '../interfaces/requests';

export const getBrukere = async (): Promise<Bruker[]> => {
  const response = await axios.get(
    `brukere`
  );
  return response.data;
};

export const addBruker = async (bruker: RegisterBrukerRequest): Promise<Bruker> => {
  const response = await axios.post(
    `bruker`,
    bruker
  );
  return response.data;
};

export const updateBruker = async (brukerId: number, bruker: RegisterBrukerRequest): Promise<Bruker> => {
  const response = await axios.put(
    `bruker/${brukerId}`,
    bruker
    );
  return response.data;
};

export const deleteBruker = async (brukerId: number): Promise<void> => {
  await axios.delete(
    `bruker/${brukerId}`
  );
};