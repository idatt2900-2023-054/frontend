import axios from './api.config';
import { Organisasjon } from '../interfaces/models';

export const getOrganisasjoner = async (): Promise<Organisasjon[]> => {
  const response = await axios.get(
    `organisasjoner`
    );
  return response.data;
};