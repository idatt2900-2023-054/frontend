import axios, { InternalAxiosRequestConfig } from 'axios';

axios.defaults.baseURL = 'https://localhost:7010';
axios.defaults.withCredentials = true;

axios.interceptors.response.use(response => {
  // Recursively search for fields named "dato" and convert them to Date objects
  function convertDates(obj: any) {
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        const value = obj[key];
        if (key === 'dato') {
          obj[key] = new Date(value);
        } else if (typeof value === 'object') {
          convertDates(value);
        }
      }
    }
  }
  
  convertDates(response.data);
  return response;
}, error => {
  console.error(error);
  if (error?.code == "ERR_NETWORK") {
    throw { message: "Nettverksfeil" }
  } else if (error?.response?.status >= 400 && error?.response?.status <= 499) {
    throw error.response.data;
  } else {
    throw { message: "Intern Serverfeil" }
  }
});

export const toDateOnlyString = (date: Date) => {
  const year = date.getUTCFullYear().toString();
  const month = (date.getUTCMonth() + 1).toString().padStart(2, "0");
  const dayOfMonth = date.getUTCDate().toString().padStart(2, "0");
  return `${year}-${month}-${dayOfMonth}`
}

axios.interceptors.request.use(request => {  
  // Recursively search for fields named "dato" and convert them to date only string
  function convertDates(obj: any) {
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        const value = obj[key];
        if (key === 'dato') {
          obj[key] = toDateOnlyString(value)
        } else if (typeof value === 'object') {
          convertDates(value);
        }
      }
    }
  }
  
  convertDates(request.data);
  return request;
});

export default axios;

export const SetLokasjonId = (lokasjonId: number) => {
  axios.interceptors.request.use((config: InternalAxiosRequestConfig) => {
    config.url = config.url?.replace("{lokasjonId}", lokasjonId.toString())
    return config;
  });
};