import { Lokasjon, Vektgruppe } from '../../interfaces/models';
import { useState } from 'react';
import { getVektgrupperOnLokasjon, deleteVektgruppe, addVektgruppe, updateVektgruppe } from '../../api/vektgruppe'
import { VektgruppeAdminForm, VektgruppeAdminFormSchema } from '../../interfaces/forms';
import Accordion from '../Accordion';
import EditableTable from '../table/EditableTable';
import TextField from '../form/TextField';
import DecimalField from '../form/DecimalField';
import DynamicTableRow from '../table/DynamicTableRow';

const Vektgrupper = (props: 
  {
    lokasjon : Lokasjon|null
  }) => {
  const [vektgrupper, setVektgrupper] = useState<Vektgruppe[]>([]);

  const onInit = async () => {
    await getVektgrupperOnLokasjon().then(setVektgrupper);
  }

  const onDelete = async (vektgruppe: Vektgruppe) => {
    await deleteVektgruppe(vektgruppe.id);
  }

  const onEdit = async (vektgruppe: Vektgruppe, vektgruppeForm: VektgruppeAdminForm) => {
    await updateVektgruppe(vektgruppe.id, {
      navn: vektgruppeForm.navn,
      defaultKassevekt: vektgruppeForm.defaultKassevekt
    })
  }

  const onAdd = async (vektgruppeForm: VektgruppeAdminForm) => {
    await addVektgruppe(vektgruppeForm)
  }

  return (
    <Accordion title="Vektgrupper" defaultExpanded={false}>
      <EditableTable
        addOnLastRow={true}
        values={vektgrupper}
        titles={["Navn", "Kassevekt",]}
        formSchema={VektgruppeAdminFormSchema}
        deleteConfirmationPrompt="Er du sikker på at du vil slette vektgruppen?"
        onInit={onInit}
        onDelete={onDelete}
        onEdit={onEdit}
        onAdd={onAdd}
        dependencies={[props.lokasjon]}
        NormalRow={props => 
          <DynamicTableRow
            id={props.value.id.toString()}
            keys={props.keys}
            values={[props.value.navn, props.value.defaultKassevekt, ...props.buttons]}
          />
        }
        EditableRow={props => 
          <DynamicTableRow
            id="EditableVektgruppe"
            keys={props.keys}
            values={[
              <TextField
              id="navn"
              control={props.control}
              defaultValue={props.value?.navn}
            />,
            <DecimalField
              id="defaultKassevekt"
              control={props.control}
              defaultValue={props.value?.defaultKassevekt}
            />,
            ...props.buttons
            ]}
          />
        }
      />
    </Accordion>
  )
}

export default Vektgrupper;