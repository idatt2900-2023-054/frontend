import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import Button from '@mui/material/Button';
import { Varegruppe, VaregruppeRecursive } from '../../interfaces/models';
import DynamicTableRow from '../table/DynamicTableRow';
import { useState } from 'react';
import { VaregruppeAdminForm } from '../../interfaces/forms';
import EditableVaregruppeTableRow from './EditableVaregruppeTableRow';
import { RowProps } from '../table/EditableTable';

const VaregruppeAccordianNasjonal = (props: {
    keys: string[],
    varegruppe: VaregruppeRecursive,
    hovedgruppe?: VaregruppeRecursive,
    varegrupper: Varegruppe[],
    recursionLevel: number,
    tableProps: RowProps<Varegruppe, VaregruppeAdminForm>
  }) => {
    const [expand, setExpand] = useState<boolean>(true);
    const paddingLeft = props.recursionLevel * 20;

    return (<>{
      (props.tableProps.editValue?.id == props.varegruppe.id) ? 
      (<EditableVaregruppeTableRow
        keys={props.keys}
        varegruppe={props.varegruppe}
        hovedgruppe={props.hovedgruppe}
        varegrupper={props.varegrupper}
        control={props.tableProps.control}
        buttons={props.tableProps.editButtons}
      />) :
      (<DynamicTableRow
        id={props.varegruppe.id.toString()}
        keys={props.keys}
        values={[
          <p style={{textAlign: "left", paddingLeft:  `${paddingLeft}px`, margin: 0}}>
            {props.varegruppe.undergrupper ? 
              <Button onClick={() => setExpand(!expand)} sx={{"&:focus": { outline: "none" }}}>
                {expand ? <ExpandLessIcon sx={{color:"#000000"}}/> : <ExpandMoreIcon sx={{color:"#000000"}}/>}
              </Button> :
              <Button disabled>
                <ExpandMoreIcon sx={{display:"none"}}/>
              </Button>
            }
            {props.varegruppe.navn}
          </p>,
          props.varegruppe.defaultKassevekt,
          ...props.tableProps.normalButtons(props.varegruppe)
        ]}
      />)
    }
    {expand && props.varegruppe.undergrupper?.map(undergruppe =>
      <VaregruppeAccordianNasjonal
        keys={props.keys}
        varegruppe={undergruppe}
        hovedgruppe={props.varegruppe}
        varegrupper={props.varegrupper}
        recursionLevel={props.recursionLevel+1}
        tableProps={props.tableProps}
      />
    )
  }</>)
}
  
export default VaregruppeAccordianNasjonal;