import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import Button from '@mui/material/Button';
import { VaregruppeLokasjonRecursive } from '../../interfaces/models';
import DynamicTableRow from '../table/DynamicTableRow';
import { useState } from 'react';
import Checkbox from '@mui/material/Checkbox';
import UseLoading from '../Loading';
import { addVaregruppeLokasjon, deleteVaregruppeLokasjon } from '../../api/varegruppe';

const VaregruppeAccordianLokal = (props: {
  keys: string[],
  varegruppe: VaregruppeLokasjonRecursive,
  recursionLevel: number,
}) => {
  const [expand, setExpand] = useState<boolean>(true);

  const { Loading, DoLoadingOperation } = UseLoading(false);

  const handleChange = async (checked: boolean) => {
    DoLoadingOperation(async () => {
      checked
      ? await addVaregruppeLokasjon(props.varegruppe.id) 
      : await deleteVaregruppeLokasjon(props.varegruppe.id)

      props.varegruppe.aktiv = checked;
    })
  };

  const paddingLeft = (props.recursionLevel * 20) + "px";
  return (<>
    <DynamicTableRow
      id={props.varegruppe.id.toString()}
      keys={props.keys}
      values={[   
        <p style={{textAlign:"left", paddingLeft:paddingLeft, margin: 0}}>
          {props.varegruppe.undergrupper ? 
          <Button onClick={() => setExpand(!expand)} sx={{"&:focus": { outline: "none" }}}>
            {expand ? <ExpandLessIcon sx={{color:"#000000"}}/> : <ExpandMoreIcon sx={{color:"#000000"}}/>}
          </Button> : 
          <Button disabled>
            <ExpandMoreIcon sx={{display:"none"}}/>
          </Button>
          }   
        {props.varegruppe.navn}
      </p>,
      props.varegruppe.defaultKassevekt,
      <Loading>
        <Checkbox onChange={e => handleChange(e.target.checked)} checked={props.varegruppe.aktiv}></Checkbox>
      </Loading>,
    ]}/>
    {expand && props.varegruppe.undergrupper?.map(undergruppe =>
      <VaregruppeAccordianLokal
        keys={props.keys}
        varegruppe={undergruppe}
        recursionLevel={props.recursionLevel+1}
      />
    )}
  </>)
}
  
export default VaregruppeAccordianLokal;