import { Varegruppe, VaregruppeRecursive } from '../../interfaces/models';
import { useState } from 'react';
import { getHovedgrupper, deleteVaregruppe, addVaregruppe, updateVaregruppe, getAllVaregrupper } from '../../api/varegruppe'
import VaregruppeAccordianNasjonal from './VaregruppeAccordianNasjonal';
import { VaregruppeAdminForm, VaregruppeAdminFormSchema } from '../../interfaces/forms';
import Accordion from '../Accordion';
import EditableTable from '../table/EditableTable';
import EditableVaregruppeTableRow from './EditableVaregruppeTableRow';

const VaregrupperNasjonal = (props: {
  setVaregrupperChanged: (varegrupperChanged: boolean) => void
}) => {
  const [hovedgrupper, setHovedgrupper] = useState<VaregruppeRecursive[]>([]);
  const [varegrupper, setVaregrupper] = useState<Varegruppe[]>([]);

  const onInit = async () => {
    await Promise.all([
      getHovedgrupper().then(setHovedgrupper),
      getAllVaregrupper().then(setVaregrupper),
    ])
  }

  const onDelete = async (varegruppe: Varegruppe) => {
    await deleteVaregruppe(varegruppe.id);
    props.setVaregrupperChanged(true);
  }

  const onEdit = async (varegruppe: Varegruppe, varegruppeForm: VaregruppeAdminForm) => {
    await updateVaregruppe(varegruppe.id, {
      ...varegruppeForm,
      hovedgruppeId: varegruppeForm.hovedgruppe?.id,
    });
    props.setVaregrupperChanged(true);
  }

  const onAdd = async (varegruppeForm: VaregruppeAdminForm) => {
    await addVaregruppe({
      ...varegruppeForm,
      hovedgruppeId: varegruppeForm.hovedgruppe?.id,
    });
    props.setVaregrupperChanged(true);
  }

  return (
    <Accordion title="Nasjonale Varegrupper" defaultExpanded={false}>
      <EditableTable
        addOnLastRow={true}
        values={hovedgrupper}
        titles={["Navn", "Kassevekt"]}
        formSchema={VaregruppeAdminFormSchema}
        deleteConfirmationPrompt="Er du sikker på at du vil slette varegruppen?"
        onInit={onInit}
        onDelete={onDelete}
        onEdit={onEdit}
        onAdd={onAdd}
        NormalRow={props => 
          <VaregruppeAccordianNasjonal
            keys={props.keys}
            varegruppe={props.value}
            varegrupper={varegrupper}
            recursionLevel={0}
            tableProps={props}
          />
        }
        EditableRow={props =>
          props.value ?
          <VaregruppeAccordianNasjonal
            keys={props.keys}
            varegruppe={props.value}
            varegrupper={varegrupper}
            recursionLevel={0}
            tableProps={props}
          /> :
          <EditableVaregruppeTableRow
            keys={props.keys}
            varegrupper={varegrupper}
            control={props.control}
            buttons={props.addButtons}
          />
        }
      />
    </Accordion>
  )
}
  
export default VaregrupperNasjonal;