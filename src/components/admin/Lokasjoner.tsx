import { Lokasjon } from '../../interfaces/models';
import { useState } from 'react';
import { deleteLokasjon, addLokasjon, updateLokasjon, getAllLokasjoner } from '../../api/lokasjon'
import { LokasjonAdminForm, LokasjonAdminFormSchema } from '../../interfaces/forms';
import Accordion from '../Accordion';
import EditableTable from '../table/EditableTable';
import TextField from '../form/TextField';
import DynamicTableRow from '../table/DynamicTableRow';

const Lokasjoner = (props:{
    lokasjoner: Lokasjon[],
    setLokasjoner: (lokasjoner: Lokasjon[]) => void
  }) => {

  const onInit = async () => {
    await getAllLokasjoner().then(props.setLokasjoner);
  }

  const onDelete = async (lokasjon: Lokasjon) => {
    await deleteLokasjon(lokasjon.id);
  }

  const onEdit = async (lokasjon: Lokasjon, lokasjonForm: LokasjonAdminForm) => {
    await updateLokasjon(lokasjon.id, lokasjonForm)
  }

  const onAdd = async (lokasjonForm: LokasjonAdminForm) => {
    await addLokasjon(lokasjonForm)
  }

  return (
    <Accordion title="Lokasjoner" defaultExpanded={false}>
      <EditableTable
        addOnLastRow={true}
        values={props.lokasjoner}
        titles={["Navn"]}
        formSchema={LokasjonAdminFormSchema}
        deleteConfirmationPrompt="Er du sikker på at du vil slette lokasjonen?"
        onInit={onInit}
        onDelete={onDelete}
        onEdit={onEdit}
        onAdd={onAdd}
        NormalRow={props => 
          <DynamicTableRow
            id={props.value.id.toString()}
            keys={props.keys}
            values={[props.value.navn, ...props.buttons]}
          />
        }
        EditableRow={props => 
          <DynamicTableRow
            id="EditableLokasjon"
            keys={props.keys}
            values={[
              <TextField
                id="navn"
                control={props.control}
                defaultValue={props.value?.navn}
              />,
              ...props.buttons
            ]}
          />
        }
      />
    </Accordion>
  )
}
  
export default Lokasjoner;
