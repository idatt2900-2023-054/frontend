import { Leverandor, Selskap } from '../../interfaces/models';
import { useEffect, useState } from 'react';
import { getLeverandorer, deleteLeverandor, addLeverandor, updateLeverandor } from '../../api/leverandor'
import { LeverandorAdminForm, LeverandorAdminFormSchema } from '../../interfaces/forms';
import { getSelskaper } from '../../api/selskap';
import EditableTable from '../table/EditableTable';
import TextField from '../form/TextField';
import Select from '../form/Select';
import Accordion from '../Accordion';
import DynamicTableRow from '../table/DynamicTableRow';
import Checkbox from '../form/Checkbox';

const Leverandorer = (props: {
  selskaperChanged: boolean,
  setSelskaperChanged: (selskaperChanged: boolean) => void
}) => {
  const [leverandorer, setLeverandorer] = useState<Leverandor[]>([]);
  const [selskaper, setSelskaper] = useState<Selskap[]>([]);

  const onInit = async () => {
    await Promise.all([
      getLeverandorer().then(setLeverandorer),
      getSelskaper().then(setSelskaper)
    ])
  }


  const onDelete = async (leverandor: Leverandor) => {
    await deleteLeverandor(leverandor.id);
  }

  const onEdit = async (leverandor: Leverandor, leverandorForm: LeverandorAdminForm) => {
    await updateLeverandor(leverandor.id, {
      ...leverandorForm,
      selskapId: leverandorForm.selskap?.id,
    });
  }

  useEffect(() => {
    if (props.selskaperChanged) {
      onInit();
      props.setSelskaperChanged(false);
    }
  }, [props.selskaperChanged])

  const onAdd = async (leverandorForm: LeverandorAdminForm) => {
    await addLeverandor({
      ...leverandorForm,
      selskapId: leverandorForm.selskap?.id,
    });
  }

  return (
    <Accordion title="Leverandører" defaultExpanded={false}>
      <EditableTable
        addOnLastRow={true}
        values={leverandorer}
        titles={["Navn", "Aktiv", "Leverandor Type", "Adresse", "Selskap"]}
        formSchema={LeverandorAdminFormSchema}
        deleteConfirmationPrompt="Er du sikker på at du vil slette leverandøren?"
        onInit={onInit}
        onDelete={onDelete}
        onEdit={onEdit}
        onAdd={onAdd}
        NormalRow={props => 
          <DynamicTableRow
            id={props.value.id.toString()}
            keys={props.keys}
            values={[props.value.navn, props.value.aktiv, props.value.leverandorType, props.value.adresse, props.value.selskap?.navn, ...props.buttons]}
          />
        }
        EditableRow={props => 
          <DynamicTableRow
            id="EditableLeverandor"
            keys={props.keys}
            values={[
              <TextField
              id="navn"
              control={props.control}
              defaultValue={props.value?.navn}
            />,
            <Checkbox
              id="aktiv"
              control={props.control}
              defaultValue={props.value?.aktiv || props.value?.aktiv == undefined && true}
            />,
            <TextField
              id="leverandorType"
              control={props.control}
              defaultValue={props.value?.leverandorType}
            />,
            <TextField
              id="adresse"
              control={props.control}
              defaultValue={props.value?.adresse}
            />,
            <Select
              id="selskap"
              control={props.control}
              options={selskaper}
              getOptionLabel={selskap => selskap.navn}
              defaultValue={props.value?.selskap}
            />,
            ...props.buttons
            ]}
          />
        }
      />
    </Accordion>
  )
}

export default Leverandorer;