import DynamicTableRow from '../table/DynamicTableRow';
import { Varegruppe } from '../../interfaces/models';
import TextField from '../form/TextField';
import { Control } from 'react-hook-form';
import { VaregruppeAdminForm } from '../../interfaces/forms';
import Select from '../form/Select';
import DecimalField from '../form/DecimalField';
import { ReactElement } from 'react';

const EditableVaregruppeTableRow = (props: {
  keys: string[],
  varegruppe?: Varegruppe,
  hovedgruppe?: Varegruppe,
  varegrupper: Varegruppe[],
  control: Control<VaregruppeAdminForm, any>,
  buttons: ReactElement[]
}) => {
  return (
    <DynamicTableRow id={"EditableVaregruppe"} keys={props.keys} values={[
      <div style={{display: "flex", flexDirection: "row", gap: "10px"}}>
        <TextField
          id="navn"
          control={props.control}
          defaultValue={props.varegruppe?.navn}
          label="Navn"
        />
        <Select
          id="hovedgruppe"
          control={props.control}
          options={props.varegrupper.filter(x => x.id !== props.varegruppe?.id)}
          getOptionLabel={varegruppe => varegruppe.navn}
          label="Hovedgruppe"
          defaultValue={props.hovedgruppe}
        />
      </div>,
      <DecimalField
        id="defaultKassevekt"
        control={props.control}
        defaultValue={props.varegruppe?.defaultKassevekt}
      />,
      ...props.buttons
    ]}/>
  )
}

export default EditableVaregruppeTableRow;