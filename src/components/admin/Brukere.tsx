import { Bruker, Lokasjon } from '../../interfaces/models';
import { useState } from 'react';
import { getBrukere, deleteBruker, addBruker, updateBruker } from '../../api/bruker'
import { BrukerAdminForm, BrukerAdminFormSchema } from '../../interfaces/forms';
import { getAllLokasjoner } from '../../api/lokasjon';
import EditableTable from '../table/EditableTable';
import TextField from '../form/TextField';
import Select from '../form/Select';
import Accordion from '../Accordion';
import DynamicTableRow from '../table/DynamicTableRow';

const Brukere = (prop:{
  lokasjoner: Lokasjon[]
}) => {
  const [brukere, setBrukere] = useState<Bruker[]>([]);

  const onInit = async () => {
    await getBrukere().then(setBrukere);
  }

  const onDelete = async (bruker: Bruker) => {
    await deleteBruker(bruker.id);
  }

  const onEdit = async (bruker: Bruker, brukerForm: BrukerAdminForm) => {
    await updateBruker(bruker.id, {
      ...brukerForm,
      lokasjonId: brukerForm.lokasjon.id,
    });
  }

  const onAdd = async (brukerForm: BrukerAdminForm) => {
    await addBruker({
      ...brukerForm,
      lokasjonId: brukerForm.lokasjon.id,
    });
  }

  return (
    <Accordion title="Brukere" defaultExpanded={false}>
      <EditableTable
        addOnLastRow={true}
        values={brukere}
        titles={["Email", "Lokasjon", "Rolle"]}
        formSchema={BrukerAdminFormSchema}
        deleteConfirmationPrompt="Er du sikker på at du vil slette brukeren?"
        onInit={onInit}
        onDelete={onDelete}
        onEdit={onEdit}
        onAdd={onAdd}
        NormalRow={props => 
          <DynamicTableRow
            id={props.value.id.toString()}
            keys={props.keys}
            values={[props.value.email, props.value.lokasjon.navn, props.value.rolle, ...props.buttons]}
          />
        }
        EditableRow={props => 
          <DynamicTableRow
            id="EditableBruker"
            keys={props.keys}
            values={[
              <TextField
              id="email"
              control={props.control}
              defaultValue={props.value?.email}
            />,
            <Select
              id="lokasjon"
              control={props.control}
              options={prop.lokasjoner}
              getOptionLabel={lokasjon => lokasjon.navn}
              defaultValue={props.value?.lokasjon}
            />,
            <Select
              id="rolle"
              control={props.control}
              options={["Bruker", "Admin", "SuperAdmin"]}
              getOptionLabel={rolle => rolle}
              defaultValue={props.value?.rolle}
            />,
            ...props.buttons
            ]}
          />
        }
      />
    </Accordion>
  )
}

export default Brukere;
