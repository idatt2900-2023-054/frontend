import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import Paper from '@mui/material/Paper';
import { Lokasjon, VaregruppeLokasjonRecursive } from '../../interfaces/models';
import DynamicTableRow from '../table/DynamicTableRow';
import VaregruppeAccordianLokal from './VaregruppeAccordianLokal';
import Accordion from '../Accordion';
import { useEffect, useState } from 'react';
import { getHovedgrupperLokasjon } from '../../api/varegruppe';
import UseLoading from '../Loading';

const VaregrupperLokal = (props: {
  varegrupperChanged: boolean,
  lokasjon : Lokasjon|null,
  setVaregrupperChanged: (varegrupperChanged: boolean) => void
}) => {
  const [varegrupper, setVaregrupper] = useState<VaregruppeLokasjonRecursive[]>([]);
  const { Loading, DoLoadingOperation } = UseLoading(true);

  const onInit = async () => {
    DoLoadingOperation(async () => {
      await getHovedgrupperLokasjon().then(setVaregrupper);
    })
  }

  useEffect(() => {
    onInit();
  }, [props.lokasjon])

  useEffect(() => {
    onInit();
    props.setVaregrupperChanged(false);
  }, [props.varegrupperChanged])

  const titles = [
    "Navn",
    "Kassevekt",
    "Bruk",
  ];

  return (
    <Accordion title="Varegrupper" defaultExpanded={false}>
      <Loading>
        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <DynamicTableRow id="head" values={titles}/>
            </TableHead>
            <TableBody>
              {varegrupper.map((varegruppe) =>
                <VaregruppeAccordianLokal
                  keys={titles}
                  varegruppe={varegruppe}
                  recursionLevel={0}
                />
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Loading>
    </Accordion>
  )
};
  
export default VaregrupperLokal;