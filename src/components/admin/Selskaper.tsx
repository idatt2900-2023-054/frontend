import { Selskap } from '../../interfaces/models';
import { useState } from 'react';
import { getSelskaper, addSelskap, updateSelskap, deleteSelskap } from '../../api/selskap'
import { SelskapAdminForm, SelskapAdminFormSchema } from '../../interfaces/forms';
import Accordion from '../Accordion';
import EditableTable from '../table/EditableTable';
import TextField from '../form/TextField';
import DynamicTableRow from '../table/DynamicTableRow';

const Selskaper = (props: {
  setSelskaperChanged: (selskaperChanged: boolean) => void
}) => {
  const [selskaper, setSelskaper] = useState<Selskap[]>([]);

  const onInit = async () => {
    await getSelskaper().then(setSelskaper);;
  }

  const onDelete = async (selskap: Selskap) => {
    await deleteSelskap(selskap.id);
    props.setSelskaperChanged(true)
  }

  const onEdit = async (selskap: Selskap, selskapForm: SelskapAdminForm) => {
    await updateSelskap(selskap.id, selskapForm)
    props.setSelskaperChanged(true)
  }

  const onAdd = async (selskapForm: SelskapAdminForm) => {
    await addSelskap(selskapForm)
    props.setSelskaperChanged(true)
  }

  return (
    <Accordion title="Selskaper" defaultExpanded={false}>
      <EditableTable
        addOnLastRow={true}
        values={selskaper}
        titles={["Navn"]}
        formSchema={SelskapAdminFormSchema}
        deleteConfirmationPrompt="Er du sikker på at du vil slette selskapet?"
        onInit={onInit}
        onDelete={onDelete}
        onEdit={onEdit}
        onAdd={onAdd}
        NormalRow={props => 
          <DynamicTableRow
            id={props.value.id.toString()}
            keys={props.keys}
            values={[props.value.navn, ...props.buttons]}
          />
        }
        EditableRow={props => 
          <DynamicTableRow
            id="EditableSelskap"
            keys={props.keys}
            values={[
              <TextField
                id="navn"
                control={props.control}
                defaultValue={props.value?.navn}
              />,
              ...props.buttons
            ]}
          />
        }
      />
    </Accordion>
  )
}
  
export default Selskaper;