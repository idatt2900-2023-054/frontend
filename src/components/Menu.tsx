import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import { Link } from 'react-router-dom';
import { useLogin } from '../localStorage';
import Box from '@mui/material/Box';
import { Logout } from '../api/login';
import { Divider } from '@mui/material';

const Menu = () => {
  const { logout, loginInfo } = useLogin();

  const adminLink = loginInfo?.bruker.rolle === "Admin" ||
    loginInfo?.bruker.rolle === "SuperAdmin" ?
    [{route: "/admin", text: "Admin"}]:
    [];

  return (
    <List sx={{position: "fixed", width: "150px", height:"100%", padding: 0, display:"flex", flexDirection:"column", borderRight: "1px solid #e0e0e0"}}>
      <Link to="" key="logo" style={{paddingLeft: 0, paddingRight: 0}}>
        <ListItem sx={{paddingLeft: 0, paddingRight: 0}}>
          <img style={{width: "130px", margin: "10px auto"}} src="https://uploads-ssl.webflow.com/5d5e9d19d7cea0cd72b43e4e/5d692228029cb212a1607e6c_Logo%20green.svg" alt="Matsentralens logo"/>
        </ListItem>
      </Link>
      <Divider/>
      {[
        {route: "/ny-innlevering", text: "Ny Innlevering"},
        {route: "/ny-utlevering", text: "Ny Utlevering"},
        {route: "/innleveringer", text: "Tidligere Innleveringer"},
        {route: "/utleveringer", text: "Tidligere Utleveringer"},
        ...adminLink
      ].map(data => (
        <Link to={data.route} key={data.text} style={{paddingLeft: 0, paddingRight: 0}}>
          <ListItem sx={{paddingLeft: 0, paddingRight: 0}}>
            <ListItemButton >
                <ListItemText primary={data.text} sx={{color: "text.primary"}}/>
            </ListItemButton>
          </ListItem>
        </Link>
      ))}
        <Box sx={{flexGrow:"1"}}/>
        <ListItem sx={{paddingLeft: 0, paddingRight: 0}}>
          <ListItemButton onClick={async () => {
            logout();
            await Logout();
          }}>
            <ListItemText primary="Logg ut" sx={{color: "text.primary"}}/>
          </ListItemButton>
        </ListItem>
    </List>
  )
}

export default Menu;