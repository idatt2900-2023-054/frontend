import {Accordion as MuiAccordion, AccordionSummary, AccordionDetails} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { useState } from 'react';

const Accordion = (props: {
  title: string,
  defaultExpanded: boolean,
  children: any;
  index?: number;
  onExpandedChange?: (expanded: boolean) => void
}) => {
  const [expanded, setExpanded] = useState<boolean>(props.defaultExpanded);

  const handleChange = () => {
    setExpanded(!expanded);
    if (props.onExpandedChange) {
      props.onExpandedChange(!expanded);
    }
  }

  return (
    <MuiAccordion expanded={expanded} onChange={() => handleChange()} sx={{backgroundColor: "#ebfaf1"}}>
      <AccordionSummary expandIcon={<ExpandMoreIcon/>} sx={{height: '80px', backgroundColor: "#ebfaf1"}}>
        <h2>{props.title}</h2>
      </AccordionSummary>
      <AccordionDetails>
        {props.children}
      </AccordionDetails>
    </MuiAccordion>
  )
}

export default Accordion;