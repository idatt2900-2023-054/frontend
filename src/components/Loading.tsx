import { CircularProgress } from "@mui/material";
import { ReactElement, useState, useContext } from "react"
import { ErrorContext } from "../ErrorContext";

const UseLoading = (deafultState: boolean) => {
  const [isLoading, setIsLoading] = useState<boolean>(deafultState);
  const { addError } = useContext(ErrorContext);

  const Loading = (props: {
    children: ReactElement,
  }) => (
    isLoading ?
    <div style={{margin: "auto", display: "flex", justifyContent: "center", alignItems: "center", overflow: "hidden"}}>
      <CircularProgress/>
    </div> :
    props.children
  );
  
  const DoLoadingOperation = async (apiCall: () => Promise<void>) => {
    setIsLoading(true);
    try {
      await apiCall().catch(addError);
    }    
    finally {
      setIsLoading(false);
    }
  };

  return {Loading, DoLoadingOperation}
}

export default UseLoading;