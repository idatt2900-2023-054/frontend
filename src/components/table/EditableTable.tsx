import { Button, Paper, Table, TableBody, TableContainer, TableHead } from "@mui/material";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import DoneIcon from '@mui/icons-material/Done';
import CloseIcon from '@mui/icons-material/Close';
import { ReactElement, useEffect, useState } from "react";
import { Control, FieldValues, FormProvider, useForm } from "react-hook-form";
import DynamicTableRow from "./DynamicTableRow";
import ConfirmationDialog from "../form/ConfirmationDialog";
import UseLoading from "../Loading";
import { yupResolver } from "@hookform/resolvers/yup";
import { ObjectSchema } from "yup";

export interface RowProps<ValueType extends object, FormType extends FieldValues> {
  keys: string[];
  editValue?: ValueType;
  control: Control<FormType, any>;
  buttons: ReactElement[];

  normalButtons: (value: ValueType) => ReactElement[];
  editButtons: ReactElement[];
  addButtons: ReactElement[];
}

export default function EditableTable<ValueType extends object, FormType extends FieldValues>(props: {
  addOnLastRow: boolean,
  values: ValueType[],
  titles: string[],
  formSchema: ObjectSchema<any, any, any, any>,
  deleteConfirmationPrompt: string,
  dependencies?: any[],

  onInit: () => Promise<void>,
  onInitEdit?: () => Promise<void>,

  onDelete: (row: ValueType) => Promise<void>,
  onEdit: (row: ValueType, form: FormType) => Promise<void>,
  onAdd?: (form: FormType) => Promise<void>,

  NormalRow: (props: RowProps<ValueType, FormType> & { value: ValueType }) => ReactElement,
  EditableRow: (props: RowProps<ValueType, FormType> & { value?: ValueType }) => ReactElement
}) {
  const [editValue, setEditValue] = useState<ValueType | undefined>(undefined);
  const [deleteValue, setDeleteValue] = useState<ValueType | undefined>(undefined);

  const { Loading, DoLoadingOperation } = UseLoading(true);

  const methods = useForm<FormType>(props.formSchema ? {resolver: yupResolver(props.formSchema)} : {});

  useEffect(() => {
    DoLoadingOperation(async () => {
      await props.onInit();
    })
  }, props.dependencies ? props.dependencies : []);

  const onCancel = () => {
    setEditValue(undefined);
    methods.unregister();
  }

  const handleDelete = async () => {
    if (deleteValue) {
      DoLoadingOperation(async () => {
        await props.onDelete(deleteValue);
        await props.onInit();
      })
      setDeleteValue(undefined);
    }
  }

  const handleEdit = (row?: ValueType) => {
    DoLoadingOperation(async () => {
      if (props.onInitEdit) {
        await props.onInitEdit();
      }

      setEditValue(row);
      methods.unregister();
    })
  }

  const onSubmit = methods.handleSubmit(async (form) => {
    DoLoadingOperation(async () => {
      if (editValue) {
        await props.onEdit(editValue, form);
      } else if (props.onAdd) {
        await props.onAdd(form)
      }
  
      await props.onInit();
  
      setEditValue(undefined);
      methods.unregister();
    })
  }, console.error);

  const normalButtons = (value: ValueType) => [
    editValue ? <div style={{height: "36px"}}/> : <Button onClick={() => handleEdit(value)}><EditIcon sx={{color:"#000000"}}/></Button>,
    editValue ? <div style={{height: "36px"}}/> : <Button onClick={() => setDeleteValue(value)}><DeleteIcon sx={{color:"#000000"}}/></Button>
  ];

  const editButtons = [
    <Button type="submit"><DoneIcon sx={{color:"#000000"}}/></Button>,
    <Button onClick={onCancel}><CloseIcon sx={{color:"#000000"}}/></Button>
  ];

  const addButtons =  [
    <Button type="submit"><DoneIcon sx={{color:"#000000"}}/></Button>,
    <Button onClick={onCancel}><CloseIcon sx={{color:"#000000"}}/></Button>
  ];

  const handleKeyDown = (e: React.KeyboardEvent<HTMLFormElement> & {target: {nodeName?: string}}) => {
    if(e.key == "Enter" && e.target?.nodeName !== 'BUTTON'){
      e.preventDefault();
    }
  }

  return (
    <>
      <Loading>
        <FormProvider {...methods}>
          <form onSubmit={onSubmit} onKeyDown={handleKeyDown}>
            <TableContainer component={Paper}>
              <Table>
                <TableHead>
                  <DynamicTableRow id="head" values={[
                    ...props.titles,
                    editValue ? "Lagre" : "Rediger",
                    editValue ? "Avbryt" : "Slett" 
                  ]}/>
                </TableHead>
                <TableBody>
                  {props.values.map((value) => 
                    editValue === value ?
                    <props.EditableRow
                      keys={props.titles}
                      value={value}
                      editValue={editValue}
                      control={methods.control}
                      buttons={editButtons}
                      normalButtons={normalButtons}
                      editButtons={editButtons}
                      addButtons={addButtons}
                    /> :
                    <props.NormalRow
                      keys={props.titles}
                      value={value}
                      editValue={editValue}
                      control={methods.control}
                      buttons={normalButtons(value)}
                      normalButtons={normalButtons}
                      editButtons={editButtons}
                      addButtons={addButtons}
                    />
                  )}
                  {editValue === undefined && props.addOnLastRow &&
                  <props.EditableRow
                    keys={props.titles}
                    control={methods.control}
                    buttons={addButtons}
                    normalButtons={normalButtons}
                    editButtons={editButtons}
                    addButtons={addButtons}
                  />}
                </TableBody>
              </Table>
            </TableContainer>
          </form>
        </FormProvider>
      </Loading>

      <ConfirmationDialog
        open={deleteValue !== undefined}
        prompt={props.deleteConfirmationPrompt}
        onCancel={() => setDeleteValue(undefined)}
        onConfirm={handleDelete}
      />
    </>
  )
}