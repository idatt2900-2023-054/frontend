import { SxProps, Theme } from '@mui/material';
import { styled } from '@mui/material/styles';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import { ReactElement } from 'react';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    fontSize: 14,
    backgroundColor: theme.palette.text.primary,
    color: theme.palette.common.white,
    padding: "5px 3px",
    border: "1px solid #1c564b",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
    color: theme.palette.common.black,
    padding: "3px",
    border: "1px solid #e8e8e8"
  }
}));

const StandardTableCell = (props: any) => (
  <StyledTableCell align="center">{props.children}</StyledTableCell>
);

const DynamicTableCell = (props: {key: any, children: ReactElement}) => {
  if (props.children === null)
  {
    return <></>;
  }

  if(typeof props.children === "boolean")
  {
    return <StandardTableCell>{props.children ? "Ja" : "Nei"}</StandardTableCell>
  }

  if(props.children instanceof Date)
  {
    return <StandardTableCell>{props.children.toLocaleDateString()}</StandardTableCell>
  }

  return <StandardTableCell>{props.children}</StandardTableCell>
};

const DynamicTableRow = (props: {id: string, values: any[], keys?: string[], sx?: SxProps<Theme>}) => (
  <TableRow key={props.id} sx={{...props.sx}}>
    {props.values.map((value, i) => <DynamicTableCell key={props.keys ? props.keys[i] : value}>{value}</DynamicTableCell>)}  
  </TableRow>
);

export default DynamicTableRow;
