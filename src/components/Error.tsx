import { Button } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import ErrorIcon from '@mui/icons-material/ErrorOutlineOutlined';
import { Error } from '../ErrorContext';

const Error = (props: {
  error: Error,
  remove: () => void
}) => {
  return (
    <div style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: "#c62828",
        padding: "10px",
        borderRadius: "10px",
        width: "50%",
      }}>
      <div style={{display: "flex", flexDirection: "row", gap: "10px", alignItems: "center"}}>
        <ErrorIcon sx={{color:"#ffffff", margin: 0}}/>
        <h3 style={{color:"#ffffff", margin: 0}}>{props.error.message}</h3>
      </div>
      <Button onClick={props.remove} sx={{color:"#ffffff", margin: 0, border: "none"}}><CloseIcon/></Button>
    </div>
  );
}

export default Error;