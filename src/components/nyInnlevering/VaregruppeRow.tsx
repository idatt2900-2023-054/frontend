import DynamicTableRow from "../table/DynamicTableRow";
import { Varegruppe } from "../../interfaces/models";
import { InnleveringForm } from "../../interfaces/forms";
import Select from "../form/Select";
import DecimalField from "../form/DecimalField";
import IntegerField from "../form/IntegerField";
import { useFormContext } from "react-hook-form";
import { useState, useEffect } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import { Button } from "@mui/material";
import TextField from "../form/TextField";

const VaregruppeRow = (props: {
  keys: string[],

  leverandorIndex: number,
  varegruppeIndex: number,

  numVaregrupper: number,
  setRemoveVaregruppeIndex: React.Dispatch<React.SetStateAction<number | undefined>>,

  varegrupper: Varegruppe[]
}) => {
  const [numVektAndKasseRows, setNumVektAndKasseRows] = useState<number>(1);
  const [nettovekt, setNettovekt] = useState<number>(0);

  const methods = useFormContext<InnleveringForm>();

  const updateNumVektAndKasseRows = () => {
    const varegruppeForm = methods.getValues(`leverandorer.${props.leverandorIndex}.varegrupper.${props.varegruppeIndex}`);
    
    let lastValidBruttovekt = -1;
    for(let i = 0; i < varegruppeForm?.bruttovekter?.length; i++) {
      if (varegruppeForm.bruttovekter[i])
      lastValidBruttovekt = i;
    }

    let lastValidKasserInn = -1;
    for(let i = 0; i < varegruppeForm?.kasserInn?.length; i++) {
      if (varegruppeForm.kasserInn[i])
      lastValidKasserInn = i;
    }

    const newLength = Math.max(lastValidBruttovekt, lastValidKasserInn) + 2;
    setNumVektAndKasseRows(newLength);
    cleanupVektAndKasse(newLength);
  };

  const cleanupVektAndKasse = (newLength: number) => {
    const varegruppeForm = methods.getValues(`leverandorer.${props.leverandorIndex}.varegrupper.${props.varegruppeIndex}`);
    
    if (varegruppeForm?.bruttovekter?.length > newLength) {
      const sliced = varegruppeForm?.bruttovekter?.slice(0, newLength);  
      methods.setValue(`leverandorer.${props.leverandorIndex}.varegrupper.${props.varegruppeIndex}.bruttovekter`, sliced);
    }

    if (varegruppeForm?.kasserInn?.length > newLength) {
      const sliced = varegruppeForm?.kasserInn?.slice(0, newLength);  
      methods.setValue(`leverandorer.${props.leverandorIndex}.varegrupper.${props.varegruppeIndex}.kasserInn`, sliced);
    }
  }

  const updateNettovekt = () => {
    const varegruppeForm = methods.getValues(`leverandorer.${props.leverandorIndex}.varegrupper.${props.varegruppeIndex}`)

    const totalBruttovekt = varegruppeForm?.bruttovekter?.map(x => x ? parseFloat(x.toString()) : 0)  
      .reduce((a, b) => a + b, 0) || 0;

    const totalKasserInn = varegruppeForm?.kasserInn?.map(x => x ? parseInt(x.toString()) : 0)  
      .reduce((a, b) => a + b, 0) || 0;

    setNettovekt(totalBruttovekt - totalKasserInn * (varegruppeForm?.kassevekt || 0));
  } 

  useEffect(() => {
    updateNumVektAndKasseRows();
    updateNettovekt();
  })

  methods.watch((innleveringForm) => {
    updateNumVektAndKasseRows();

    const varegruppeForm = innleveringForm?.leverandorer?.at(props.leverandorIndex)?.varegrupper?.at(props.varegruppeIndex);      
    const kassevekt = varegruppeForm?.kassevekt;
    const defaultKassevekt = varegruppeForm?.varegruppe?.defaultKassevekt;
    if (defaultKassevekt && kassevekt == undefined) {
      methods.setValue(`leverandorer.${props.leverandorIndex}.varegrupper.${props.varegruppeIndex}.kassevekt`, defaultKassevekt);
    }

    updateNettovekt();
  })

  return (
    <DynamicTableRow
      id={`leverandorer.${props.leverandorIndex}.varegrupper.${props.varegruppeIndex}`}
      keys={props.keys}
      values={[
        <Select<Varegruppe>
          id={`leverandorer.${props.leverandorIndex}.varegrupper.${props.varegruppeIndex}.varegruppe`}
          control={methods.control}
          options={props.varegrupper}
          getOptionLabel={varegruppe => varegruppe.navn}
        />,
        <div style={{display: "flex", flexDirection:"column"}}>
          {Array.from({length: numVektAndKasseRows}).map((x, i) =>
            <DecimalField id={`leverandorer.${props.leverandorIndex}.varegrupper.${props.varegruppeIndex}.bruttovekter.${i}`} control={methods.control}/>
          )}
        </div>,
        <div style={{display: "flex", flexDirection:"column"}}>
          {Array.from({length: numVektAndKasseRows}).map((x, i) =>
            <IntegerField
              id={`leverandorer.${props.leverandorIndex}.varegrupper.${props.varegruppeIndex}.kasserInn.${i}`}
              control={methods.control}
            />
          )}
        </div>,
        <DecimalField
          id={`leverandorer.${props.leverandorIndex}.varegrupper.${props.varegruppeIndex}.kassevekt`}
          control={methods.control}
        />,
        <p>{nettovekt.toFixed(2)}</p>,
        <TextField
          id={`leverandorer.${props.leverandorIndex}.varegrupper.${props.varegruppeIndex}.notat`}
          control={methods.control}
          multiline={true}
        />,
        (props.numVaregrupper > 1 && props.varegruppeIndex != props.numVaregrupper - 1 ?
        <Button onClick={() => props.setRemoveVaregruppeIndex(props.varegruppeIndex)}>
          <DeleteIcon sx={{color:"#000000"}}/>
          </Button>:
        null)
      ]}/>
  )
};
  
export default VaregruppeRow;