import { Button, Table, TableBody, TableContainer, TableHead } from "@mui/material";
import DynamicTableRow from "../table/DynamicTableRow";
import Paper from '@mui/material/Paper';
import { Leverandor, Varegruppe } from "../../interfaces/models";
import { InnleveringForm } from "../../interfaces/forms";
import Checkbox from "../form/Checkbox";
import Select from "../form/Select";
import { useFormContext } from "react-hook-form";
import VaregruppeTable from "./VaregruppeTable";
import { useEffect, useState } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import Accordion from "../Accordion";

const LeverandorTable = (props: {
  leverandorIndex: number,
  numLeverandorer: number,
  setRemoveLeverandorIndex: React.Dispatch<React.SetStateAction<number>>,
  leverandorer: Leverandor[],
  varegrupper: Varegruppe[]
  expanded: boolean
  onExpandedChange: (index: number, expanded: boolean) => void
}) => {
  const { control, watch, getValues } = useFormContext<InnleveringForm>();
  const [leverandorNavn, setLeverandorNavn] = useState<string>(`leverandør ${props.leverandorIndex + 1}`);

  const updateLeverandorNavn = () => {
    const leverandorNavn = getValues(`leverandorer.${props.leverandorIndex}.leverandor.navn`);
    if (leverandorNavn) {
      setLeverandorNavn(leverandorNavn);
    } else {
      setLeverandorNavn(`leverandør ${props.leverandorIndex + 1}`);
    }  
  }

  watch(() => {
    updateLeverandorNavn();
  })

  useEffect(() => {
    updateLeverandorNavn();
  }, []);

  const titles = [
    "Leverandør",
    "Ren donasjon",
    "Primær"
  ];

  return (
    <Accordion
      title={"Varer fra " + leverandorNavn}
      defaultExpanded={props.expanded}
      index={props.leverandorIndex}
      onExpandedChange={(expanded) => props.onExpandedChange(props.leverandorIndex, expanded)}
    >
      <TableContainer component={Paper} sx={{marginTop:"10px", width: "80%"}}>
        <Table>
          <TableHead>
            <DynamicTableRow id="head" values={[
              ...titles,
              (props.numLeverandorer > 1 ? "Fjern" : null)
            ]}/>
          </TableHead>
          <TableBody>
            <DynamicTableRow
              id={`leverandorer.${props.leverandorIndex}`}
              keys={titles}
              values={[
                <Select
                  id={`leverandorer.${props.leverandorIndex}.leverandor`}
                  control={control}
                  options={props.leverandorer}
                  getOptionLabel={leverandor => leverandor.navn}
                />,
                <Checkbox
                  id={`leverandorer.${props.leverandorIndex}.renDonasjon`}
                  control={control}
                />,
                <Checkbox
                  id={`leverandorer.${props.leverandorIndex}.primaer`}
                  control={control}
                />,
                (props.numLeverandorer > 1 ?
                <Button onClick={() => props.setRemoveLeverandorIndex(props.leverandorIndex)}>
                  <DeleteIcon sx={{color:"#000000"}}/>
                  </Button> : 
                null)
              ]}/>
          </TableBody>
        </Table>
      </TableContainer>
      <div style={{width: "1px", height:"25px"}}/>
      <VaregruppeTable leverandorIndex={props.leverandorIndex} varegrupper={props.varegrupper}/>
    </Accordion>
  );
};
  
export default LeverandorTable;