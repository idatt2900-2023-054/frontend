import { Button, Dialog, DialogActions, DialogTitle, Table, TableBody, TableContainer, TableHead } from "@mui/material";
import DynamicTableRow from "../table/DynamicTableRow";
import Paper from '@mui/material/Paper';
import { Varegruppe } from "../../interfaces/models";
import { InnleveringForm } from "../../interfaces/forms";
import { useFormContext } from "react-hook-form";
import { useState, useEffect } from "react";
import VaregruppeRow from "./VaregruppeRow";

const VaregruppeTable = (props: {
  leverandorIndex: number,
  varegrupper: Varegruppe[]
}) => {
    const methods = useFormContext<InnleveringForm>();
    const [numVaregrupper, setNumVaregrupper] = useState<number>(1);
    const [removeVaregruppeIndex, setRemoveVaregruppeIndex] = useState<number | undefined>(undefined);

    const updateNumVaregrupper = () => {
      const varegruppeForms = methods.getValues(`leverandorer.${props.leverandorIndex}.varegrupper`);
      let lastValidVaregruppe = -1;
      for(let i = 0; i < varegruppeForms?.length; i++) {
        if (varegruppeForms[i]?.varegruppe)
          lastValidVaregruppe = i;
      }
      const newLength = lastValidVaregruppe + 2;
      setNumVaregrupper(newLength);
      cleanupVaregrupper(newLength);
    };

    const cleanupVaregrupper = (newLength: number) => {
      const varegruppeForms = methods.getValues(`leverandorer.${props.leverandorIndex}.varegrupper`)
      if (varegruppeForms?.length > newLength) {
        const sliced = varegruppeForms.slice(0, newLength);  
        methods.setValue(`leverandorer.${props.leverandorIndex}.varegrupper`, sliced);
      }
    }

    useEffect(() => {
      updateNumVaregrupper();
    })

    methods.watch(() => {
      updateNumVaregrupper();
    })

    const handleRemove = () => {
      for (let i = removeVaregruppeIndex!; i < numVaregrupper; i++) {
        const values = methods.getValues(`leverandorer.${props.leverandorIndex}.varegrupper.${i + 1}`);

        methods.unregister(`leverandorer.${props.leverandorIndex}.varegrupper.${i}`);
        methods.setValue(`leverandorer.${props.leverandorIndex}.varegrupper.${i}`, values);
      }

      setRemoveVaregruppeIndex(undefined);
      const newLength = numVaregrupper - 1;
      setNumVaregrupper(newLength);
      cleanupVaregrupper(newLength);
    }

    const titles = [
      "Varegruppe",
      "Bruttovekt [kg]",
      "Kasser inn",
      "Kassevekt [kg]",
      "Nettovekt [kg]",
      "Notat"
    ];

    return (
      <>
        <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <DynamicTableRow id="head" values={[
              ...titles,
              (numVaregrupper > 1 ? "Fjern" : null)
            ]}/>
          </TableHead>
          <TableBody>
            {Array.from({length: numVaregrupper}).map((x, i) => 
              <VaregruppeRow
                key={i}
                keys={titles}
                leverandorIndex={props.leverandorIndex}
                varegruppeIndex={i}
                numVaregrupper={numVaregrupper}
                setRemoveVaregruppeIndex={setRemoveVaregruppeIndex}
                varegrupper={props.varegrupper}
              />
            )}
          </TableBody>
        </Table>
      </TableContainer>

      <Dialog open={removeVaregruppeIndex != undefined}>
        <DialogTitle>Er du sikker på at du vil fjerne varegruppen?</DialogTitle>
        <DialogActions>
          <Button onClick={() => setRemoveVaregruppeIndex(undefined)} color="error" variant="contained">Nei</Button>
          <Button onClick={() => handleRemove()} color="success" variant="contained">Ja</Button>
        </DialogActions>
      </Dialog>
    </>
    );
  };
  
export default VaregruppeTable;