import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers"
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns"
import { nb } from "date-fns/locale"

const DateRange = (props: {
  startDate: Date,
  setStartDate: (newValue: Date) => void,
  endDate: Date,
  setEndDate: (newValue: Date) => void
}) => {
  return (
    <div style={{ display: "flex", flexDirection: "row", alignItems: "center", gap: "10px"}}>
      <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={nb} >
        <DesktopDatePicker
          label="Fra"
          value={props.startDate}
          onChange={newValue => newValue && props.setStartDate(newValue)}
          sx={{minWidth: "150px", width: "150px"}}
        />
        <h2 style={{margin: 0}}>-</h2>
        <DesktopDatePicker
          label="Til"
          value={props.endDate}
          onChange={newValue => newValue && props.setEndDate(newValue)}
          sx={{minWidth: "150px", width: "150px"}}
        />
      </LocalizationProvider>
    </div>
  )
}

export default DateRange;