import { TextField, Autocomplete } from "@mui/material";
import { Control, Controller } from "react-hook-form";

export default function Select<T> (props: {
    id: string,
    control: Control<any, any>
    options: T[],
    getOptionLabel: ((option: T) => string)
    defaultValue?: T,
    label?: string
  }) {
  return (
    <Controller
      name={props.id}
      control={props.control}
      defaultValue={props.defaultValue}
      render={({ field: { onChange, value }, fieldState: { error } }) => (
        <Autocomplete<T | undefined>
          disablePortal
          id={props.id}
          fullWidth
          autoHighlight
          options={props.options}
          getOptionLabel={value => value ? props.getOptionLabel(value) : ""}
          value={value}
          onChange={(event, selectedValue) => onChange(selectedValue)}
          sx={{minWidth:"250px", width: "100%", color: "black", ".MuiOutlinedInput-notchedOutline": { border: "1px solid" }}}
          size="small"
          renderInput={(params) =>
            <TextField
              {...params}
              label={props.label}
              error={!!error}
              helperText={error?.message}
              sx={{input: {color: "black"}}}
              variant="outlined"
            />}
        />
      )}
    />
  )
}