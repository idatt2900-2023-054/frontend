import { TextField as MuiTextField, SxProps, Theme } from "@mui/material";
import { Control, Controller } from "react-hook-form";

export default function TextField (props: {
    id: string,
    control: Control<any, any>
    defaultValue?: string,
    label?: string,
    multiline?: boolean,
    sx?: SxProps<Theme>
  }) {
  return (
    <Controller
      name={props.id}
      control={props.control}
      defaultValue={props.defaultValue}
      render={({ field: { onChange, value }, fieldState: { error } }) => (
        <MuiTextField
          id={props.id}
          type="text"
          fullWidth
          value={value || ""}
          onChange={onChange}
          error={!!error}
          helperText={error?.message}
          label={props.label}
          size="small"
          sx={{...props.sx, width:"100%", minWidth: "150px", input: {color: "black"}, ".MuiOutlinedInput-notchedOutline": { border: "1px solid" }}}
          multiline={props.multiline}
        />
      )}
    />
  )
}