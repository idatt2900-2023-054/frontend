import { Checkbox as MuiCheckbox } from "@mui/material";
import { Control, Controller } from "react-hook-form";

export default function Checkbox (props: {
    id: string,
    control: Control<any, any>
    defaultValue?: boolean,
  }) {
  return (
    <Controller
      name={props.id}
      control={props.control}
      defaultValue={props.defaultValue || false}
      render={({ field: { onChange, value } }) => (
        <MuiCheckbox
          id={props.id}
          value={value || false}
          checked={value}
          onChange={onChange}
          size="medium"
        />
      )}
    />
  )
}