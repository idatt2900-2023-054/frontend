import { DesktopDatePicker } from "@mui/x-date-pickers";
import { Control, Controller } from "react-hook-form";
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { nb } from 'date-fns/locale'

export default function Datepicker (props: {
    id: string,
    control: Control<any, any>
    defaultValue?: Date,
  }) {

  if (nb && nb.options) {
    nb.options.weekStartsOn = 1
  }

  return (
    <Controller
      name={props.id}
      control={props.control}
      defaultValue={props.defaultValue}
      render={({ field: { onChange, value }, fieldState: { error }  }) => (
        <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={nb}>
          <DesktopDatePicker
            onChange={onChange}
            value={value || ""}
            sx={{minWidth: "150px", width: "150px", input: {color: "black"}, ".MuiOutlinedInput-notchedOutline": { border: "1px solid" }}}
            slotProps={{
              textField: {
                error: !!error,
                helperText: error?.message
              }
            }}
          />
        </LocalizationProvider>
      )}
    />
  )
}