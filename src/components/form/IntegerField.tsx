import { TextField } from "@mui/material";
import { Control, Controller } from "react-hook-form";

export default function IntegerField (props: {
    id: string,
    control: Control<any, any>
    defaultValue?: number,
  }) {
  return (
    <Controller
      name={props.id}
      control={props.control}
      defaultValue={props.defaultValue}
      render={({ field: { onChange, onBlur, value }, fieldState: { error }  }) => (
        <TextField
          id={props.id}
          type="number"
          value={value}
          onChange={onChange}
          onBlur={onBlur}
          autoComplete='off'
          inputProps={{
            min: 0,
          }}
          error={!!error}
          helperText={error?.message}
          size="small"
          sx={{width:"100%", minWidth: "80px", ".MuiOutlinedInput-notchedOutline": { border: "1px solid" } }}
        />
      )}
    />
  )
}