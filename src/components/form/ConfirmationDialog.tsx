import { Button, Dialog, DialogActions, DialogTitle } from '@mui/material';

export default function ConfirmationDialog (props: {
    open: boolean,
    prompt: string,
    onCancel: () => void,
    onConfirm: () => void,
  }) {
  return (
    <Dialog open={props.open}>
    <DialogTitle>{props.prompt}</DialogTitle>
    <DialogActions>
      <Button onClick={props.onCancel} color="error" variant="contained">Nei</Button>
      <Button onClick={props.onConfirm} color="success" variant="contained">Ja</Button>
    </DialogActions>
  </Dialog>
  )
}