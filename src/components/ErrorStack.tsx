import Error from './Error';
import { useContext } from 'react';
import { ErrorContext } from '../ErrorContext';

const ErrorStack = () => {
  const { errors, removeError } = useContext(ErrorContext);

  return (
    <div style={{
      position: "fixed",
      width: "calc(100% - 150px)",
      marginLeft: "150px",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      gap: "10px",
      marginTop: "10px",
      zIndex: 1
    }}>
      {errors.map((error, index) => 
        <Error error={error} remove={() => removeError(index)}/>
      )}
  </div>
  );
}

export default ErrorStack;