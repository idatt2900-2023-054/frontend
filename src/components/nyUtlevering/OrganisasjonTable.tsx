import { Button, Table, TableBody, TableContainer, TableHead } from "@mui/material";
import DynamicTableRow from "../table/DynamicTableRow";
import Paper from '@mui/material/Paper';
import { Organisasjon, Vektgruppe } from "../../interfaces/models";
import { UtleveringForm } from "../../interfaces/forms";
import Checkbox from "../form/Checkbox";
import Select from "../form/Select";
import IntegerField from "../form/IntegerField";
import { useFormContext } from "react-hook-form";
import VektgruppeTable from "./VektgruppeTable";
import { useEffect, useState } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import Accordion from "../Accordion";

const OrganisasjonTable = (props: {
  organisasjonIndex: number,
  numOrganisasjoner: number,
  setRemoveOrganisasjonIndex: React.Dispatch<React.SetStateAction<number>>,
  organisasjoner: Organisasjon[],
  vektgrupper: Vektgruppe[]
  expanded: boolean
  onExpandedChange: (index: number, expanded: boolean) => void

}) => {
  const { control, watch, getValues } = useFormContext<UtleveringForm>();
  const [organisasjonNavn, setOrganisasjonNavn] = useState<string>(`organisasjon ${props.organisasjonIndex + 1}`);

  const updateOrganisasjonNavn = () => {
    const organisasjonNavn = getValues(`organisasjoner.${props.organisasjonIndex}.organisasjon.navn`);
    if (organisasjonNavn) {
      setOrganisasjonNavn(organisasjonNavn);
    } else {
      setOrganisasjonNavn(`organisasjon ${props.organisasjonIndex + 1}`);
    }  
  }

  watch(() => {
    updateOrganisasjonNavn();
  })

  useEffect(() => {
    updateOrganisasjonNavn();
  }, []);

  const titles = [
    "Organisasjon",
    "Primær",
    "Kasser inn"
  ];

  return (
    <Accordion
      title={"Varer til " + organisasjonNavn}
      defaultExpanded={props.expanded}
      index={props.organisasjonIndex}
      onExpandedChange={(expanded) => props.onExpandedChange(props.organisasjonIndex, expanded)}
    >
      <TableContainer component={Paper} sx={{marginTop:"10px", width: "80%"}}>
        <Table>
          <TableHead>
            <DynamicTableRow id="head" values={[
              ...titles,
              (props.numOrganisasjoner > 1 ? "Fjern" : null)
            ]}/>
          </TableHead>
          <TableBody>
            <DynamicTableRow
              id={`organisasjoner.${props.organisasjonIndex}`}
              keys={titles}
              values={[
                <Select
                  id={`organisasjoner.${props.organisasjonIndex}.organisasjon`}
                  control={control}
                  options={props.organisasjoner}
                  getOptionLabel={organisasjon => organisasjon.navn}
                />,
                <Checkbox
                  id={`organisasjoner.${props.organisasjonIndex}.primaer`}
                  control={control}
                />,
                <IntegerField
                  id={`organisasjoner.${props.organisasjonIndex}.kasserInn`}
                  control={control}
                />,
                (props.numOrganisasjoner > 1 ?
                <Button onClick={() => props.setRemoveOrganisasjonIndex(props.organisasjonIndex)}>
                  <DeleteIcon sx={{color:"#000000"}}/>
                </Button> :
                null)
              ]}/>
          </TableBody>
        </Table>
      </TableContainer>
      <div style={{width: "1px", height:"25px"}}/>
      <VektgruppeTable organisasjonIndex={props.organisasjonIndex} vektgrupper={props.vektgrupper}/>
    </Accordion>
  );
};
  
export default OrganisasjonTable;