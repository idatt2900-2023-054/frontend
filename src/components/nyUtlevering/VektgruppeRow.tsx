import DynamicTableRow from "../table/DynamicTableRow";
import { Vektgruppe } from "../../interfaces/models";
import { UtleveringForm } from "../../interfaces/forms";
import Select from "../form/Select";
import DecimalField from "../form/DecimalField";
import IntegerField from "../form/IntegerField";
import { useFormContext } from "react-hook-form";
import { useState, useEffect } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import { Button } from "@mui/material";
import TextField from "../form/TextField";

const VektgruppeRow = (props: {
  keys: string[],

  organisasjonIndex: number,
  vektgruppeIndex: number,

  numVektgrupper: number,
  setRemoveVektgruppeIndex: React.Dispatch<React.SetStateAction<number | undefined>>,

  vektgrupper: Vektgruppe[]
}) => {
  const [numVektAndKasseRows, setNumVektAndKasseRows] = useState<number>(1);
  const [nettovekt, setNettovekt] = useState<number>(0);

  const methods = useFormContext<UtleveringForm>();

  const updateNumVektAndKasseRows = () => {
    const vektgruppeForm = methods.getValues(`organisasjoner.${props.organisasjonIndex}.vektgrupper.${props.vektgruppeIndex}`);
    
    let lastValidBruttovekt = -1;
    for(let i = 0; i < vektgruppeForm?.bruttovekter?.length; i++) {
      if (vektgruppeForm.bruttovekter[i])
      lastValidBruttovekt = i;
    }

    let lastValidKasserUt = -1;
    for(let i = 0; i < vektgruppeForm?.kasserUt?.length; i++) {
      if (vektgruppeForm.kasserUt[i])
      lastValidKasserUt = i;
    }

    const newLength = Math.max(lastValidBruttovekt, lastValidKasserUt) + 2;
    setNumVektAndKasseRows(newLength);
    cleanupVektAndKasse(newLength);
  };

  const cleanupVektAndKasse = (newLength: number) => {
    const vektgruppeForms = methods.getValues(`organisasjoner.${props.organisasjonIndex}.vektgrupper.${props.vektgruppeIndex}`);
    
    if (vektgruppeForms?.bruttovekter?.length > newLength) {
      const sliced = vektgruppeForms?.bruttovekter?.slice(0, newLength);  
      methods.setValue(`organisasjoner.${props.organisasjonIndex}.vektgrupper.${props.vektgruppeIndex}.bruttovekter`, sliced);
    }

    if (vektgruppeForms?.kasserUt?.length > newLength) {
      const sliced = vektgruppeForms?.kasserUt?.slice(0, newLength);  
      methods.setValue(`organisasjoner.${props.organisasjonIndex}.vektgrupper.${props.vektgruppeIndex}.kasserUt`, sliced);
    }
  }

  const updateNettovekt = () => {
    const varegruppeForm = methods.getValues(`organisasjoner.${props.organisasjonIndex}.vektgrupper.${props.vektgruppeIndex}`)

    const totalBruttovekt = varegruppeForm?.bruttovekter?.map(x => x ? parseFloat(x.toString()) : 0)  
      .reduce((a, b) => a + b, 0) || 0;

    const totalKasserUt = varegruppeForm?.kasserUt?.map(x => x ? parseInt(x.toString()) : 0)  
      .reduce((a, b) => a + b, 0) || 0;

    setNettovekt(totalBruttovekt - totalKasserUt * (varegruppeForm?.kassevekt || 0));
  } 

  useEffect(() => {
    updateNumVektAndKasseRows();
    updateNettovekt();
  })

  methods.watch((utleveringForm) => {
    updateNumVektAndKasseRows();
    
    const vektgruppeForm = utleveringForm?.organisasjoner?.at(props.organisasjonIndex)?.vektgrupper?.at(props.vektgruppeIndex);      
    
    const kassevekt = vektgruppeForm?.kassevekt;
    const defaultKassevekt = vektgruppeForm?.vektgruppe?.defaultKassevekt;
    if (defaultKassevekt && kassevekt == undefined) {
      methods.setValue(`organisasjoner.${props.organisasjonIndex}.vektgrupper.${props.vektgruppeIndex}.kassevekt`, defaultKassevekt);
    }
    
    updateNettovekt();
  })

  return (
    <DynamicTableRow
      id={`organisasjoner.${props.organisasjonIndex}.vektgrupper.${props.vektgruppeIndex}`}
      keys={props.keys}
      values={[
        <Select<Vektgruppe>
          id={`organisasjoner.${props.organisasjonIndex}.vektgrupper.${props.vektgruppeIndex}.vektgruppe`}
          control={methods.control}
          options={props.vektgrupper}
          getOptionLabel={vektgruppe => vektgruppe.navn}
        />,
        <div style={{display: "flex", flexDirection:"column"}}>
          {Array.from({length: numVektAndKasseRows}).map((x, i) =>
            <DecimalField id={`organisasjoner.${props.organisasjonIndex}.vektgrupper.${props.vektgruppeIndex}.bruttovekter.${i}`} control={methods.control}/>
          )}
        </div>,
        <div style={{display: "flex", flexDirection:"column"}}>
          {Array.from({length: numVektAndKasseRows}).map((x, i) =>
            <IntegerField
              id={`organisasjoner.${props.organisasjonIndex}.vektgrupper.${props.vektgruppeIndex}.kasserUt.${i}`}
              control={methods.control}
            />
          )}
        </div>,
        <DecimalField id={`organisasjoner.${props.organisasjonIndex}.vektgrupper.${props.vektgruppeIndex}.kassevekt`} control={methods.control}/>,
        <p>{nettovekt.toFixed(2)}</p>,
        <TextField
          id={`organisasjoner.${props.organisasjonIndex}.vektgrupper.${props.vektgruppeIndex}.notat`}
          control={methods.control}
          multiline={true}
        />,
        (props.numVektgrupper > 1 && props.vektgruppeIndex != props.numVektgrupper - 1 ?
        <Button onClick={() => props.setRemoveVektgruppeIndex(props.vektgruppeIndex)}>
          <DeleteIcon sx={{color:"#000000"}}/>
          </Button> :
        null)
      ]}/>
  )
};
  
export default VektgruppeRow;