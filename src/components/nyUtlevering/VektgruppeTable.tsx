import { Button, Dialog, DialogActions, DialogTitle, Table, TableBody, TableContainer, TableHead } from "@mui/material";
import DynamicTableRow from "../table/DynamicTableRow";
import Paper from '@mui/material/Paper';
import { Vektgruppe } from "../../interfaces/models";
import { UtleveringForm } from "../../interfaces/forms";
import { useFormContext } from "react-hook-form";
import { useState, useEffect } from "react";
import VektgruppeRow from "./VektgruppeRow";

const VektgruppeTable = (props: {
  organisasjonIndex: number,
  vektgrupper: Vektgruppe[]
}) => {
    const methods = useFormContext<UtleveringForm>();
    const [numVektgrupper, setNumVektgrupper] = useState<number>(1);
    const [removeVektgruppeIndex, setRemoveVektgruppeIndex] = useState<number | undefined>(undefined);

    const updateNumVektgrupper = () => {
      const vektgruppeForms = methods.getValues(`organisasjoner.${props.organisasjonIndex}.vektgrupper`);
      let lastValidVektgruppe = -1;
      for(let i = 0; i < vektgruppeForms?.length; i++) {
        if (vektgruppeForms[i]?.vektgruppe)
          lastValidVektgruppe = i;
      }
      const newLength = lastValidVektgruppe + 2;
      setNumVektgrupper(newLength);
      cleanupVektgrupper(newLength);
    };

    const cleanupVektgrupper = (newLength: number) => {
      const vektgruppeForms = methods.getValues(`organisasjoner.${props.organisasjonIndex}.vektgrupper`)
      if (vektgruppeForms?.length > newLength) {
        const sliced = vektgruppeForms.slice(0, newLength);  
        methods.setValue(`organisasjoner.${props.organisasjonIndex}.vektgrupper`, sliced);
      }
    }

    useEffect(() => {
      updateNumVektgrupper();
    })

    methods.watch(() => {
      updateNumVektgrupper();
    })

    const handleRemove = () => {
      for (let i = removeVektgruppeIndex!; i < numVektgrupper; i++) {
        const values = methods.getValues(`organisasjoner.${props.organisasjonIndex}.vektgrupper.${i + 1}`);

        methods.unregister(`organisasjoner.${props.organisasjonIndex}.vektgrupper.${i}`);
        methods.setValue(`organisasjoner.${props.organisasjonIndex}.vektgrupper.${i}`, values);
      }

      setRemoveVektgruppeIndex(undefined);
      const newLength = numVektgrupper - 1;
      setNumVektgrupper(newLength);
      cleanupVektgrupper(newLength);
    }

    const titles = [
      "Vektgruppe",
      "Bruttovekt [kg]",
      "Kasser ut",
      "Kassevekt [kg]",
      "Nettovekt [kg]",
      "Notat",
    ];

    return (
      <>
        <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <DynamicTableRow id="head" values={[
              ...titles,
              (numVektgrupper > 1 ? "Fjern" : null)
            ]}/>
          </TableHead>
          <TableBody>
            {Array.from({length: numVektgrupper}).map((x, i) => 
              <VektgruppeRow
                key={i} 
                keys={titles}
                organisasjonIndex={props.organisasjonIndex}
                vektgruppeIndex={i}
                numVektgrupper={numVektgrupper}
                setRemoveVektgruppeIndex={setRemoveVektgruppeIndex}
                vektgrupper={props.vektgrupper}
              />
            )}
          </TableBody>
        </Table>
      </TableContainer>

      <Dialog open={removeVektgruppeIndex != undefined}>
        <DialogTitle>Er du sikker på at du vil fjerne vektgruppen?</DialogTitle>
        <DialogActions>
          <Button onClick={() => setRemoveVektgruppeIndex(undefined)} color="error" variant="contained">Nei</Button>
          <Button onClick={() => handleRemove()} color="success" variant="contained">Ja</Button>
        </DialogActions>
      </Dialog>
    </>
    );
  };
  
export default VektgruppeTable;