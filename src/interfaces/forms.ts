import { Leverandor, Lokasjon, Organisasjon, Varegruppe, Vektgruppe, Selskap } from "./models";
import * as yup from 'yup';

const requiredText = "Obligatorisk felt";
const positiveText = "Verdien må være et positivt tall";

const requiredPositiveNumber = () => yup.number().transform(value => {
  if (value == "" || value == undefined || Number.isNaN(value))
    return 0;
  return value;
}).min(0, positiveText).required(positiveText)

const optionalPositiveNumber = () => yup.number().transform(value => {
  if (value == "" || value == undefined || Number.isNaN(value))
    return 0;
  return value;
}).min(0, positiveText)

const notEmptyString = () => yup.string().transform(value => {
  const trimmed = value?.trim();
  return trimmed != "" ? trimmed : undefined; 
});

const arrayExceptLast = () => yup.array().transform((arr: any[]) => {
  return arr.length > 1 ? arr.slice(0, -1) : arr;
});

const arrayAtLeastOne = () => yup.array().transform((arr: any[]) => {
  const filtered = arr.filter(x => x != undefined && x != null && !Number.isNaN(x) && x != "")
  return filtered.length > 0 ? filtered : arr.slice(0, 1);
});

export interface InnleveringForm {
  dato: Date;
  leverandorer: LeverandorForm[];
}

export interface LeverandorForm {
  leverandor: Leverandor;
  renDonasjon: boolean;
  primaer: boolean;
  varegrupper: VaregruppeForm[];
}

export interface VaregruppeForm {
  varegruppe: Varegruppe;
  bruttovekter: number[];
  kasserInn: number[];
  kassevekt: number;
  notat?: string;
}

export const VaregruppeFormSchema = yup.object().shape({
  varegruppe: yup.object().required(requiredText),
  bruttovekter:arrayAtLeastOne().of(requiredPositiveNumber()),
  kasserInn: arrayExceptLast().of(optionalPositiveNumber()),
  kassevekt: optionalPositiveNumber(),
  notat: notEmptyString().optional(),
});

export const LeverandorFormSchema = yup.object().shape({
  leverandor: yup.object().required(requiredText),
  renDonasjon: yup.boolean().required(requiredText),
  primaer: yup.boolean().required(requiredText),
  varegrupper: arrayExceptLast().of(VaregruppeFormSchema)
});

export const InnleveringFormSchema = yup.object().shape({
  dato: yup.date().required(requiredText),
  leverandorer: yup.array().of(LeverandorFormSchema).required(requiredText)
});

export interface UtleveringForm {
  dato: Date;
  organisasjoner: OrganisasjonForm[];
}

export interface OrganisasjonForm {
  organisasjon: Organisasjon;
  primaer: boolean;
  kasserInn: number;
  vektgrupper: VektgruppeForm[];
}

export interface VektgruppeForm {
  vektgruppe: Vektgruppe;
  bruttovekter: number[];
  kasserUt: number[];
  kassevekt: number;
  notat?: string;
}

export const VektgruppeFormSchema = yup.object().shape({
  vektgruppe: yup.object().required(requiredText),
  bruttovekter:arrayAtLeastOne().of(requiredPositiveNumber()),
  kasserUt: arrayExceptLast().of(optionalPositiveNumber()),
  kassevekt: optionalPositiveNumber(),
  notat: notEmptyString().optional(),
});

export const OrganisasjonFormSchema = yup.object().shape({
  organisasjon: yup.object().required(requiredText),
  primaer: yup.boolean().required(requiredText),
  kasserInn: optionalPositiveNumber(),
  vektgrupper: arrayExceptLast().of(VektgruppeFormSchema),
});

export const UtleveringFormSchema = yup.object().shape({
  dato: yup.date().required(requiredText),
  organisasjoner: yup.array().of(OrganisasjonFormSchema).required(requiredText)
});

export interface EditInnleveringForm {
  dato: Date;
  leverandor: Leverandor;
  varegruppe: Varegruppe;
  bruttovekt: number;
  kasserInn: number;
  kassevekt: number;
  renDonasjon: boolean;
  primaer: boolean;
  notat?: string;
}

export const EditInnleveringFormSchema = yup.object().shape({
  dato: yup.date().required(requiredText),
  leverandor: yup.object().required(requiredText),
  varegruppe: yup.object().required(requiredText),
  bruttovekt: requiredPositiveNumber(),
  kasserInn: optionalPositiveNumber(),
  kassevekt: optionalPositiveNumber(),
  renDonasjon: yup.boolean().required(requiredText),
  primaer: yup.boolean().required(requiredText),
  notat: notEmptyString().optional()
});

export interface EditUtleveringForm {
  dato: Date;
  organisasjon: Organisasjon;
  vektgruppe: Vektgruppe;
  bruttovekt: number;
  kasserUt: number;
  kassevekt: number;
  kasserInn: number;
  primaer: boolean;
  notat?: string;
}

export const EditUtleveringFormSchema = yup.object().shape({
  dato: yup.date().required(requiredText),
  organisasjon: yup.object().required(requiredText),
  vektgruppe: yup.object().required(requiredText),
  bruttovekt: requiredPositiveNumber(),
  kasserUt: optionalPositiveNumber(),
  kassevekt: optionalPositiveNumber(),
  kasserInn: optionalPositiveNumber(),
  primaer: yup.boolean().required(requiredText),
  notat: notEmptyString().optional()
});

export interface BrukerAdminForm {
  email: string;
  lokasjon: Lokasjon;
  rolle: string;
}

export const BrukerAdminFormSchema = yup.object().shape({
  email: notEmptyString().email("Må være gyldig email").required(requiredText),
  lokasjon: yup.object().required(requiredText),
  rolle: notEmptyString().required(requiredText)
});

export interface VektgruppeAdminForm {
  navn: string;
  defaultKassevekt: number;
}

export const VektgruppeAdminFormSchema = yup.object().shape({
  navn: notEmptyString().required(requiredText),
  defaultKassevekt: optionalPositiveNumber()
});

export interface LokasjonAdminForm {
  navn: string;
}

export const LokasjonAdminFormSchema = yup.object().shape({
  navn: notEmptyString().required(requiredText)
});

export interface VaregruppeAdminForm {
  navn: string;
  hovedgruppe?: Varegruppe;
  defaultKassevekt: number;
}

export const VaregruppeAdminFormSchema = yup.object().shape({
  navn: notEmptyString().required(requiredText),
  hovedgruppe: yup.object().optional(),
  defaultKassevekt: optionalPositiveNumber()
});

export interface SelskapAdminForm {
  navn: string;
}

export const SelskapAdminFormSchema = yup.object().shape({
  navn: notEmptyString().required(requiredText)
});

export interface LeverandorAdminForm {
  navn: string;
  aktiv: boolean;
  leverandorType: string;
  adresse: string;
  selskap?: Selskap;
}

export const LeverandorAdminFormSchema = yup.object().shape({
  navn: notEmptyString().required(requiredText),
  aktiv: yup.boolean().required(requiredText),
  leverandorType: notEmptyString().required(requiredText),
  adresse: notEmptyString().required(requiredText),
  selskap: yup.object().optional().nullable()
});