export interface Bruker {
    id: number;
    email: string;
    lokasjon: Lokasjon;
    rolle: string;
}

export interface LoginInfo {
    bruker: Bruker;
    expires: string
}

export interface Innlevering {
    id: number;
    dato: Date;
    leverandor: Leverandor;
    varegruppe: Varegruppe;
    bruttovekt: number;
    kasserInn: number;
    kassevekt: number;
    nettovekt: number;
    renDonasjon: boolean;
    primaer: boolean;
    notat?: string;
    lokasjon: Lokasjon;
}

export interface Varegruppe {
    id: number;
    navn: string;
    hovedgruppeId?: number;
    defaultKassevekt: number;
}

export interface VaregruppeRecursive {
    id: number;
    navn: string;
    undergrupper?: VaregruppeRecursive[];
    defaultKassevekt: number;
}

export interface VaregruppeLokasjonRecursive {
    id: number;
    navn: string;
    undergrupper?: VaregruppeLokasjonRecursive[];
    aktiv: boolean;
    defaultKassevekt: number;
}

export interface Leverandor {
    id: number;
    navn: string;
    aktiv: boolean;
    leverandorType: string;
    adresse: string;
    selskap?: Selskap;
}

export interface Selskap {
    id: number;
    navn: string;
}

export interface Lokasjon {
    id: number;
    navn: string;
}

export interface Utlevering {
    id: number;
    dato: Date;
    organisasjon: Organisasjon
    vektgruppe: Vektgruppe;
    bruttovekt: number;
    kasserUt: number;
    kassevekt: number;
    nettovekt: number;
    kasserInn: number;
    primaer: boolean;
    notat?: string;
    lokasjon: Lokasjon;
}

export interface Organisasjon {
    id: number;
    navn: string;
}

export interface Vektgruppe {
    id: number;
    navn: string;
    lokasjon: Lokasjon;
    defaultKassevekt: number;
}
