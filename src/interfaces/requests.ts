export interface RegisterBrukerRequest {
    email: string;
    lokasjonId: number;
    rolle: string;
}

export interface RegisterLokasjonRequest{
    navn: string
} 

export interface RegisterInnleveringRequest {
    dato: Date;

    leverandorId: number;
    renDonasjon: boolean;
    primaer: boolean;

    varegruppeId: number;
    bruttovekt: number;
    kasserInn: number;
    kassevekt: number;
    notat?: string;
}

export interface RegisterUtleveringRequest {
    dato: Date;

    organisasjonId: number;
    primaer: boolean;
    kasserInn: number;

    vektgruppeId: number;
    bruttovekt: number;
    kasserUt: number;
    kassevekt: number;
    notat?: string;
}

export interface RegisterVektgruppeRequest {
    navn: string;
    defaultKassevekt: number;
}

export interface RegisterSelskapRequest {
    navn: string;
}

export interface RegisterLeverandorRequest {
    navn: string;
    aktiv: boolean;
    leverandorType: string;
    adresse: string; 
    selskapId?: number;
}

export interface RegisterVaregruppeRequest {
    navn: string;
    hovedgruppeId?: number;
    defaultKassevekt: number;
}