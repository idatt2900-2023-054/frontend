import { createContext, useState } from 'react';

export interface Error {
  message: string
}

export const ErrorContext = createContext<{
  errors: Error[],
  addError: (error: Error) => void,
  removeError: (index: number) => void
}>({
  errors: [],
  addError: () => {},
  removeError: () => {}
});

const ErrorContextProvider = (props: {children: any[]}) => {
  const [errors, setErrors] = useState<Error[]>([]);
  
  const addError = (error: Error) => {
    setErrors([...errors, error])
  }
  
  const removeError = (index: number) => {
    setErrors(errors.filter((e, i) => i !== index));
  }

  return (
    <ErrorContext.Provider value={{
      errors: errors,
      addError: addError,
      removeError: removeError
    }}>
      {...props.children}
    </ErrorContext.Provider>
  )
}
  
export default ErrorContextProvider;